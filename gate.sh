ADDR=$1

if [ "$ADDR" == "" ]; then
	ADDR=`~/docker-ip.sh ngp`
fi

while [ TRUE ]
do
	./ngpgate \
		-log_dir /tmp \
		-blAddr $ADDR:50051 \
		-pathToStatic ~/go/src/bitbucket.org/Axxonsoft/ngpgate/static
	sleep 3s
done
