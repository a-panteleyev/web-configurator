/* eslint-disable import/no-extraneous-dependencies */
const merge = require('webpack-merge');

const base = require('./webpack.base');

const dev = {
	mode: 'development',
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					cacheDirectory: true,
					plugins: ['react-hot-loader/babel'],
				},
			},
		],
	},
	devtool: 'eval-source-map',
	devServer: {
		compress: true,
		proxy: [
			{
				target: 'http://localhost:8081',
				context: ['/v1'],
			},
		],
		historyApiFallback: true,
		port: 3000,
	},
};

module.exports = merge(base, dev);
