/* eslint-disable import/no-extraneous-dependencies */
const merge = require('webpack-merge');

const base = require('./webpack.base');

const prod = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ],
  },
};

module.exports = merge(base, prod);
