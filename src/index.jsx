// @flow
import 'babel-polyfill';
import * as React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

import App from 'containers/App';
import LanguageProvider from 'containers/LanguageProvider';
import store from 'store';
import {lightTheme, darkTheme} from 'theme';

import 'typeface-roboto';

const root = document.getElementById('app');
const theme = createMuiTheme(lightTheme);

if (!root) {
	throw new Error('Can not find #app');
}

render(
	<Provider store={store}>
		<MuiThemeProvider theme={theme}>
			<LanguageProvider>
				<Router>
					<Route path="/" component={App} />
				</Router>
			</LanguageProvider>
		</MuiThemeProvider>
	</Provider>,
	root
);
