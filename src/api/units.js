// @flow

import {
	PFX_CFG,
	queryUrl,
	arrayParams,
	handleResponse,
	handleError,
} from 'api/common';
import type {Changeset, Unit} from 'containers/Units/types';

export const fetchUnits = (uids: Array<string> | empty): Promise<Unit> =>
	fetch(queryUrl(`${PFX_CFG}`, arrayParams('unit_uids', uids)))
		.then(handleResponse)
		.then((data: Unit) => ({data}))
		.catch(handleError);

export const postChanges = (changes: Changeset): Promise<Changeset> =>
	fetch(queryUrl(`${PFX_CFG}`), {method: 'POST', body: JSON.stringify(changes)})
		.then(handleResponse)
		.then((data: Changeset) => ({data}))
		.catch(handleError);
