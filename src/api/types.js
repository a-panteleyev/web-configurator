// @flow

export type Response = {|
	[string]: mixed,
	error: string,
|};
