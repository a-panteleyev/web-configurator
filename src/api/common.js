// @flow

import constant from 'lodash/fp/constant';
import size from 'lodash/fp/size';
import times from 'lodash/fp/times';
import zip from 'lodash/fp/zip';
import omitBy from 'lodash/fp/omitBy';
import isNil from 'lodash/fp/isNil';

export const omitNil = omitBy(isNil);
export const arrayParams = (name: string, params: Array<*>) =>
	zip(times(constant(name), size(params)), params);
export const handleError = (error: Error) => Promise.resolve({error});
export const handleJson = (response: Response): Promise<Object> => response.json;
export const handleText = (response: Response): Promise<string> => response.text;

const resolve = val => Promise.resolve(val);
const reject = val => Promise.reject(val);

export function handleResponse(response: Response) {
	const contentType: string = response.headers.get('content-type') || '';
	const passStatus = response.ok ? resolve : reject;

	switch(contentType) {
		case 'application/json':
			return passStatus(response.json());
		case 'plain/text':
			return passStatus(response.text());
		default:
			return Promise.reject(
				new Error(`Unknown response type: ${contentType}`)
			);
	}
}
const V1 = '/v1';
export const PFX_CFG = `${V1}/configurator`;
export const queryUrl = (url: string, query: $TupleMap<string, *>): URL => {
	const urlObj: URL = new URL(url, window.location);

	urlObj.search = new URLSearchParams(query).toString();

	return urlObj;
};
