import {PFX_CFG, handleResponse, handleError, queryUrl} from 'api/common';

const STRIPPED = 'VIEW_MODE_STRIPPED';
const FULL = 'VIEW_MODE_FULL';

export const getTemplates = (stripped: bool) =>
	fetch(
		queryUrl(`${PFX_CFG}/templates`, {
			view: stripped ? STRIPPED : FULL
		}), {
			method: 'GET',
		}
	)
	.then(handleResponse)
	.catch(handleError);

export const getTemplatesById = () =>
	fetch(
		`${PFX_CFG}/templates:barchGet`, {
			method: 'GET',
		}
	)
	.then(handleResponse)
	.catch(handleError);

export const postTmplChanges = (changes: Object): Promise<Object> =>
	fetch(
		`${PFX_CFG}/templates`,
		{
			method: 'POST',
			body: JSON.stringify(changes)
		}
	)
	.then(handleResponse)
	.then(changeset => ({changeset}))
	.catch(handleError);

export const postTmplAssign = (changes: Object): Promise<Object> =>
	fetch(
		`${PFX_CFG}/assignments`,
		{
			method: 'POST',
			body: JSON.stringify(changes)
		}
	)
	.then(handleResponse)
	.then(changeset => ({changeset}))
	.catch(handleError);

