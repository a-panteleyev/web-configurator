// @flow

import flow from 'lodash/fp/flow';
import isEqual from 'lodash/fp/isEqual';
import negate from 'lodash/fp/negate';
import size from 'lodash/fp/size';

export const only: (Array<any>) => boolean = flow([size, isEqual(1)]);
export const several: (Array<any>) => boolean = flow([
	size,
	negate(isEqual(0)),
]);
