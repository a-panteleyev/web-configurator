// @flow

import keyBy from 'lodash/fp/keyBy';

export const keyById = keyBy(({id}) => id);

export default {keyById};
