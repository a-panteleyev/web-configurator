// @flow

import * as React from 'react';

export function useAnchor(defVal: HTMLElement | null = null) {
	const [anchor, setAnchor] = React.useState(defVal);
	const handleAnchor = React.useCallback(
		(e: SyntheticMouseEvent<HTMLElement>) => setAnchor(e.currentTarget),
		[]
	);
	const resetAnchor = React.useCallback(() => setAnchor(defVal), []);

	return {
		anchor,
		handleAnchor,
		resetAnchor,
	};
}

export default {useAnchor};
