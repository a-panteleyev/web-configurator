// @flow

import curry from 'lodash/fp/curry';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import getFp from 'lodash/fp/get';
import join from 'lodash/fp/join';
import map from 'lodash/fp/map';
import reduce from 'lodash/fp/reduce';
import setFp from 'lodash/fp/set';
import split from 'lodash/fp/split';
import startsWith from 'lodash/fp/startsWith';
import takeWhile from 'lodash/fp/takeWhile';
import unset from 'lodash/fp/unset';
import values from 'lodash/fp/values';

import {resolveUnit} from 'utils/units';
import type {UnitType, UnitList, Path, Pid} from 'containers/UnitType/types';
import type {Item} from 'components/UnitMaker/types';

// TODO: tests

const prepend: (value: *) => (Array<*>) => Array<*> = curry(
	(val: *, arr: Array<*>) => [val, ...arr]
);
const propPath: Path => Path = flow([
	join('.properties.'),
	split('.'),
	prepend('properties'),
]);

const isUid: Pid => boolean = startsWith('hosts/');

function resolveSubUnit(unit: UnitType, path: Path): {unit: Path, prop: Path} {
	const pathUnits = takeWhile(isUid, path);
	const pathProp = path.slice(pathUnits.length);
	const result = resolveUnit(pathUnits, unit);

	return {unit: result.resolv, prop: pathProp};
}

function hydratePath(unit: UnitType, path: Path, id: Pid): Path {
	const resolved = resolveSubUnit(unit, path);

	return [...resolved.unit, ...propPath([...resolved.prop, id])];
}

export function set(unit: UnitType, path: Path, item: Item) {
	return setFp(hydratePath(unit, path, item.id), item, unit);
}

export function rm(unit: UnitType, path: Path, id: Pid) {
	return unset(hydratePath(unit, path, id), unit);
}

export function get(unit: UnitType, path: Path, id: Pid) {
	return getFp(hydratePath(unit, path, id), unit);
}

export function toList({
	properties,
	units,
	...rest
}: {
	properties: {[string]: Item},
	units: Array<UnitType>,
	rest: {[string]: mixed},
}) {
	return {
		...rest,
		properties: values(properties).map(toList),
		units: map(toList, units),
	};
}

// TODO: investigate {units : []} appearance in PropertyItem
function indexify(coll, item) {
	return {
		...coll,
		[item.id]: toMap(item),
	};
}

export function toMap({
	properties,
	units,
	...rest
}: {
	properties: {[string]: Item},
	units: UnitList,
	rest: {[string]: mixed},
}) {
	return {
		...rest,
		properties: reduce(indexify, {}, properties),
		units: map(toMap, units), // TODO: also used for properties. perhaps, added here
	};
}

const getProps = getFp('properties');

export function mergeDiff(unit: UnitType, diff: UnitType) {
	return {
		...unit,
		...diff,
		properties: {...getProps(unit), ...getProps(diff)},
	};
}

export function findProp(propName: string): UnitType => * {
	return flow([
		getFp('properties'),
		find(({id}) => id === propName),
		(prop = {}) => prop[`value_${prop.type}`],
	]);
}

export default {set, get, rm, toList, toMap};
