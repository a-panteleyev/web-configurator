// @flow

import curry from 'lodash/fp/curry';
import findIndex from 'lodash/fp/findIndex';
import flatMap from 'lodash/fp/flatMap';
import flow from 'lodash/fp/flow';
import getFp from 'lodash/fp/get';
import getOr from 'lodash/fp/getOr';
import isEqual from 'lodash/fp/isEqual';
import isNil from 'lodash/fp/isNil';
import map from 'lodash/fp/map';
import omitBy from 'lodash/fp/omitBy';
import reduce from 'lodash/fp/reduce';
import size from 'lodash/fp/size';
import update from 'lodash/fp/update';
import without from 'lodash/fp/without';
import type {Factory, Unit, Path, Pid} from 'containers/Units/types';

export const flatUnit = ({
	units,
	status,
	factory,
	...unit
}: Unit): Array<Unit> => [
	{
		...unit,
		units: [],
	},
	...flatMap(flatUnit, units),
];

export const cloneStructure = ({
	units,
	status,
	factory,
	properties,
	...unit
}: Unit): Unit => ({
	...unit,
	properties: [],
	units: map(cloneStructure, units),
});

const getUnits: Unit => Array<Unit> = getOr([], 'units');
const getUid: Unit => Pid = getFp('uid');

export const resolveUnit = curry((path: Path, unit: Unit) =>
	reduce(
		({resolv, curUnit}, pathFrag) => {
			const unitIdx = flow([
				getUnits,
				findIndex(flow([getUid, isEqual(pathFrag)])),
			])(curUnit);

			return {
				resolv: unitIdx !== -1 ? [...resolv, 'units', unitIdx] : [],
				curUnit: getOr(curUnit, ['units', unitIdx], curUnit),
			};
		},
		{resolv: [], curUnit: unit},
		path
	)
);

const removeUndefined = omitBy(isNil);
export function sanitiseUnit({uid, type, properties, units}: Unit): Unit {
	return {
		uid,
		type,
		properties,
		units: map(sanitiseUnit, removeUndefined(units)),
	};
}

const getFactory = getOr([], 'factory');
// eslint-disable-next-line no-use-before-define
const getDeepFactories = flow([getUnits, flatMap(getFactories)]);
export function getFactories(unit: Unit): Array<Factory> {
	return [
		...getFactory(unit).map(factory => ({
			...factory,
			ctx: `${unit.type}.${unit.display_id}`,
			parent: unit.uid,
		})),
		...getDeepFactories(unit),
	];
}
export default {flatUnit};
