import * as log from 'loglevel';

log.enableAll();

export default log;
