// @flow
import {createReducer} from 'redux-act';
import flow from 'lodash/fp/flow';
import set from 'lodash/fp/set';
import * as actions from './actions';

export type Status = 'ok' | null;

type State = {
	loading: boolean,
	domainInit: boolean,
};

const initialState: State = {
	loading: false,
	domainInit: false,
};

export default createReducer({
	[actions.loading]: set('loading', true),
	[actions.domainInit]: set('domainInit', true),
	[actions.ready]: flow([
		set('loading', false),
		set('domainInit', false),
	]),
}, initialState);
