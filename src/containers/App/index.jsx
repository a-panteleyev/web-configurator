// @flow
import * as React from 'react';
import {compose, bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {hot} from 'react-hot-loader';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import isEqual from 'lodash/fp/isEqual';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';

import Nodes, {unitType, withType} from 'containers/Units';
import {SyncTemplates} from 'containers/Templates';
import Bar from 'components/Bar';
import Blotbox from 'components/Blotbox';
import ListUnits from 'components/ListUnits';
import UnitFactory from 'components/UnitFactory';
import Search from 'components/Search';
import Tabs, {TAB_CAMS, TAB_STORAGE, TAB_NODES} from 'components/Tabs';

import type {Status} from './reducer';

type Props = {
	children: React.Node,
	status: Status,
	checkStatus: () => void,
};

type State = {
	tab: string,
};

const styles = theme => ({
	'@global': {
		body: {
			backgroundColor: theme.palette.background.default,
			display: 'flex',
			flexDirection: 'column',
			margin: 0,
			position: 'relative',
		},
		'#app': {
			display: 'flex',
			flexDirection: 'column',
			justifyContent: 'space-between',
			height: '100vh',
			width: '100vw',
			overflow: 'hidden',
		},
	},
	blotbox: {
		position: 'absolute',
		top: theme.spacing.unit * 4,
		right: theme.spacing.unit * 2,
		zIndex: 1101,
	},
});

const tabTypes = {
	[TAB_CAMS]: unitType.CAM,
	[TAB_STORAGE]: unitType.STORAGE,
	[TAB_NODES]: unitType.NODE,
};

// const App = ({ children, status, checkStatus }: Props) => (
class App extends React.Component<Props, State> {
	state = {
		tab: TAB_CAMS,
	};

	handleTabChange = (event, tab) => this.setState(() => ({tab}));

	render() {
		const {tab} = this.state;
		const {children, classes} = this.props;
		const type = tabTypes[tab];

		return (
			<>
				<AppBar position="static" color="default">
					<Toolbar>
						<Grid container direction="column" alignItems="center">
							<Search />
						</Grid>
					</Toolbar>
				</AppBar>
				<Bar />
				<Nodes />
				<SyncTemplates />
				<ListUnits type={type} />
				<Tabs value={tab} onChange={this.handleTabChange} />
				<Blotbox className={classes.blotbox}>
					<UnitFactory type={type} />
				</Blotbox>
			</>
		);
	}
}

// const selectGlobal = ({ global: { status, tab } }) => ({ status, tab });
// const injectActions = dispatch => bindActionCreators({setTab}, dispatch);

// export const withGlobal = connect(selectGlobal, injectActions);
export default compose(
	hot(module),
	// connect(
	//   selectStatus,
	//   injectActions,
	// ),
	withStyles(styles)
)(App);
