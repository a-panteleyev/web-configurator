// @flow

import * as React from 'react';

type Props = {};
type State = {
	selected: Set<string>,
};

export default function (Component) {
	return class withSelection extends React.Component<Props, State> {
		state = {
			selected: new Set(),
		}
		select = (id: string) => this.setState(({selected}) => ({
			selected: selected.add(id),
		}));
		deselect = (id: string) => this.setState(({selected}) => ({
			selected: selected.delete(id) && selected,
		}));
		render() {
			const {props, state, ...methods} = this;

			return <Components {...props} {...state} {...methods} />
		}
	}
}
