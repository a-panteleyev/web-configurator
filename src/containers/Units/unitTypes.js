export const DOMAIN = 'Domain';

export const CAM = 'DeviceIpint';
export const CAM_CHAN = 'VideoChannel';
export const CAM_STREAM = 'Streaming';
export const CAM_MIC = 'Microphone';
export const CAM_STOR = 'EmbeddedStorage';
export const CAM_AVDETECT = 'AVDetector';
export const CAM_ARCH = 'ArchiveContext';
export const CAM_PTZ = 'Telemetry';
export const CAM_IO = 'IODevice';
export const CAM_SPK = 'Speaker';

export const NODE = 'Node';
export const STORAGE = 'MultimediaStorage';
export const STORAGE_CTX = 'ArchiveContext';
export const DETECTOR = 'AVDetector';
