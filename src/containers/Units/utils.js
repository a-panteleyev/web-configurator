// @flow

import curry from 'lodash/fp/curry';
import forEach from 'lodash/fp/forEach';
import flow from 'lodash/fp/flow';
import fpMap from 'lodash/fp/map';
import flatten from 'lodash/fp/flatten';
import flatMap from 'lodash/fp/flatMap';
import filter from 'lodash/fp/filter';
import get from 'lodash/fp/get';
import negate from 'lodash/fp/negate';
import placeholder from 'lodash/fp/placeholder';
import reduce from 'lodash/fp/reduce';
import isNil from 'lodash/fp/isNil';
import isEqual from 'lodash/fp/isEqual';
import set from 'lodash/fp/set';
import tap from 'lodash/fp/tap';

import type {Uid, Unit, UnitList, UnitIndex} from './types';

type Wrapper = {
	path: Array<number>,
	unit: Unit,
};
const map = fpMap.convert({cap: false});
const getUnits = get('units');
const getUnitId = get(['unit', 'uid']);
const levelDown = flow(
	fpMap(({unit: root, path}) =>
		map((unit, idx) => ({unit, path: [...path, 'units', idx]}))(getUnits(root))
	),
	flatten,
	filter(negate(isNil))
);
export const unitBfs = (
	unit: Unit,
	searchUid: string
): Array<string> | void => {
	console.time('bfs');
	let items: Array<Wrapper> = [{path: [], unit}];
	let idx: number;

	do {
		items = levelDown(items);
		idx = items.findIndex(flow([getUnitId, isEqual(searchUid)]));
	} while (idx === -1 && items.length > 0);

	const ret = get([idx, 'path'])(items);

	console.timeEnd('bfs');

	return ret;
};
export function flatUnits({units, ...unit}, list = []) {
	list.push(unit);
	if (units) {
		units.forEach(u => flatUnits(u, list));
	}

	return list;
}
export const selectNodes = flow([
	getUnits, // root
	flatMap(getUnits), // domains
	flatMap(getUnits), // nodes
]);
export const selectSvc = flow([
	selectNodes,
	flatMap(getUnits), // services
]);
export const selectType = type =>
	flow([
		flatUnits,
		filter(
			flow(
				get('type'),
				isEqual(type)
			)
		),
	]);
export const selectUid_ = curry((state, uid: Uid) =>
	get(unitBfs(state, uid), state)
);
export const selectTree = get(['units', 'tree']);
export const selectUid = curry((index: UnitIndex, uid: Uid) => index.get(uid));
export const selectArchCtx = (cameraRef: string) =>
	flow([
		flatUnits,
		filter(
			flow([
				get(['properties', 'camera_ref', 'value_string']),
				isEqual(cameraRef),
			])
		),
	]);
export const getUnitsByUid_ = curry((uids: Array<Uid>, units: UnitList) =>
	map(selectUid(units), uids)
);
export const getUnitsByUid = curry((uids: Array<Uid>, units: UnitList) => {
	const uidSet = new Set(uids);

	return flow([flatUnits, filter(({uid}: Unit) => uidSet.has(uid))])(units);
});
export const getStrippedNodes = flow([
	selectNodes,
	filter(negate(isNil)),
	filter(get('stripped')),
]);

export const indexUnitAdd = (index: UnitIndex, unit: Unit) => {
	index.set(unit.uid, unit);
	reduce(indexUnitAdd, index)(unit.units);

	return index;
};

export const indexUnitRm = (index: UnitIndex, unit: Unit) => {
	index.delete(unit.uid);
	reduce(indexUnitRm, index)(unit.units);

	return index;
};

export const parentify = curry((parent: Unit, unit: Unit) => {
	unit.parent = parent;
	forEach(parentify(unit), unit.units);
});

const forEach2 = forEach.convert({cap: false});
export const pathify = (path: Path, unit: Unit) => {
	unit.path = path;
	forEach2(
		(unit: Unit, idx: number) => pathify([...path, 'units', idx], unit),
		unit.units
	);
};
