// @flow
import {createAction} from 'redux-act';
import {toList, toMap} from 'utils/props';

import type {
	RequestUnitAction,
	FetchUnitAction,
	ChangeUnitAction,
	UpdateUnitAction,
	AddUnitAction,
	RmUnitAction,
} from './types';

export const unit = {
	REQUEST: 'unit/REQUEST',
	CHANGE: 'unit/CHANGE',
	UPDATE: 'unit/UPDATE',
	FETCH: 'unit/FETCH',
	ADD: 'unit/ADD',
	REMOVE: 'unit/REMOVE',
};

export const requestUnit: RequestUnitAction = createAction(unit.REQUEST);

// toMap converts array of properties to "index" like object
export const updateUnit: UpdateUnitAction = createAction(unit.UPDATE, toMap);
export const fetchUnit: FetchUnitAction = createAction(unit.FETCH);
// toList converts back to array supported by API
export const addUnit: AddUnitAction = createAction(unit.ADD, toList);
export const changeUnit: ChangeUnitAction = createAction(unit.CHANGE, toList);
export const rmUnit: RmUnitAction = createAction(unit.REMOVE, toList);

export const getServices = createAction('cfg/GET_SERVICES');
