// @flow
import * as React from 'react';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import join from 'lodash/fp/join';
import map from 'lodash/fp/map';
import {compose, bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
	selectNodes,
	selectSvc,
	selectType,
	selectUid,
	getUnitsByUid,
} from './utils';
import * as actions from './actions';
import * as unitType from './unitTypes';
import type {UnitIndex, UnitList, Uid} from './types';
// TODO: Learn how to import all types

type Props = {
	getServices: Function,
};

class Nodes extends React.Component<Props> {
	componentDidMount() {
		const {getServices} = this.props;

		getServices();
	}

	render() {
		return null;
	}
}
export {unitType};

export const PARENT_FOR = {
	[unitType.CAM]: unitType.NODE,
	[unitType.STORAGE]: unitType.NODE,
	[unitType.NODE]: unitType.DOMAIN,
};

const mapActionsToProps = (dispatch: Function) =>
	bindActionCreators(actions, dispatch);

export const withNodes = connect(
	({units: {tree: units}}) => ({nodes: selectNodes(units)}),
	mapActionsToProps
);
export const withSvc = connect(
	({units: {tree: units}}) => ({services: selectSvc(units)}),
	mapActionsToProps
);

const makePropAmalgam = flow([map(get('value_string')), join('\0')]);
export const withType = (type: unitType) =>
	connect(
		({search, units: {tree: units}}) => ({
			units: selectType(type)(units).filter(({display_id, properties}) =>
				`${display_id}\0${makePropAmalgam(properties)}`
					.toLowerCase()
					.includes(search.toLowerCase())
			),
			type,
		}),
		mapActionsToProps
	);

const selectUnit = Component => (props: {index: UnitIndex, uid: Uid}) => (
	<Component unit={selectUid(props.index, props.uid)} {...props} />
);

export const withUnits = connect(
	({units: {tree: units, index}}) => ({units, index}),
	mapActionsToProps
);
export const withUnit = compose(
	withUnits,
	selectUnit
);
const selectUnitSet = Component => (props: {
	index: UnitIndex,
	uids: Array<Uid>,
}) => <Component unitSet={getUnitsByUid(props.index, props.uids)} {...props} />;
export const withUnitSet = compose(
	withUnits,
	selectUnitSet
);
export const getUnitByUid = selectUid;
export default withNodes(Nodes);
