// @flow
import {createReducer} from 'redux-act';
import assign from 'lodash/fp/assign';
import get from 'lodash/fp/get';
import reduce from 'lodash/fp/reduce';
import update from 'lodash/fp/update';
import set from 'lodash/fp/set';

import {indexUnitAdd, indexUnitRm, parentify, pathify, unitBfs} from './utils';
import {updateUnit} from './actions';
import type {Unit, Uid, UnitUpdatePayload} from './types';

type State = {
	tree: {
		units: Array<Unit>,
	},
	index: Map<Uid, Unit>,
};

export const ROOT = {
	type: 'root',
	uid: 'root',
	units: [],
	parent: {},
	path: ['tree', 'units', 0],
};
const tree = {
	units: [ROOT],
	path: ['tree'],
};
ROOT.parent = tree;
const initialState: State = {
	tree,
	index: new Map([[ROOT.uid, ROOT], [ROOT.parent, tree]]),
};

// TODO: index all units for fast look up
export default createReducer(
	{
		[updateUnit]: (state: Unit, {units}: UnitUpdatePayload) =>
			reduce(
				(state_, unit) => {
					// const path = unitBfs(state_, getParent(unit));
					// const parentUnit = getParent(unit);
					const {path} = state_.index.get(unit.uid);

					console.time('indexify');
					parentify(unit.parent, unit);
					pathify(path, unit);
					indexUnitRm(state_.index, unit);
					indexUnitAdd(state_.index, unit);
					console.timeEnd('indexify');
					// TODO: find and fill in parent unit
					return set(path, unit)(state_);
				},
				state,
				units
			),
	},
	initialState
);
