// @flow

import {delay} from 'redux-saga';
import {takeEvery, takeLatest, put, call, select} from 'redux-saga/effects';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import isEqual from 'lodash/fp/isEqual';
import isObject from 'lodash/fp/isObject';
import join from 'lodash/fp/join';
import keyBy from 'lodash/fp/keyBy';
import keys from 'lodash/fp/keys';
import map from 'lodash/fp/map';
import placeholder from 'lodash/fp/placeholder';
import set from 'lodash/fp/set';
import size from 'lodash/fp/size';
import split from 'lodash/fp/split';
import take from 'lodash/fp/take';
import tap from 'lodash/fp/tap';
import uniqBy from 'lodash/fp/uniqBy';
import update from 'lodash/fp/update';
import * as api from 'api/units';
import logger from 'utils/logger';
import {sanitiseUnit} from 'utils/units';

import * as appActions from 'containers/App/actions';
import type {Saga} from 'redux-saga';
import {ROOT} from './reducer';
import * as actions from './actions';
import {getStrippedNodes, selectUid, selectTree} from './utils';

import type {Changeset, Unit, UnitList, RequestUnitAction} from './types';

const SECOND = 1e3;

function* applyUnitChangeset(changeset: Changeset): Saga<void> {
	const response: {changes: string, error: string} = yield call(
		api.postChanges,
		changeset
	);

	if (response.error) {
		logger.error('Unit change failed:', response.error);
	}
	logger.debug(response.changes);

	return response;
}

function* initDomain({
	units: [
		{
			uid,
			factory: [domain],
		},
	],
}): Saga<void> {
	const setDomainName = set(['properties', '0', 'value_string'], 'domain');
	const added = [
		{
			uid,
			units: [setDomainName(domain)],
		},
	];

	yield put(appActions.domainInit());

	yield* applyUnitChangeset({added});
}

const keyByUid = keyBy(get('uid'));
const adoptUnits = unitMap => unit => ({
	...unit,
	parent: unitMap[unit.uid].parent,
});

function* getUnits({
	payload: reqUnits,
}: {
	payload: RequestParams,
}): Saga<UnitList | empty> {
	logger.debug('Fetching:', reqUnits);
	yield put(appActions.loading());

	const reqUnitsMap = keyByUid(reqUnits);
	const uids = keys(reqUnitsMap);
	const {data, error}: {data?: Unit, error?: string} = yield call(
		api.fetchUnits,
		uids
	);

	logger.debug('Got:', data);

	if (!data || error) {
		logger.error('Unit fetch failed');

		return;
	}
	if (data.not_found_objects) {
		logger.log('Not found', data.not_found_objects);

		return;
	}
	if (data.unreachable_objects) {
		logger.log('Not reachable', data.unreachable_objects);

		return;
	}

	const adoptedUnits = update('units', map(adoptUnits(reqUnitsMap)), data);

	yield put(appActions.ready());
	yield put(actions.updateUnit(adoptedUnits));

	return adoptedUnits;
}

const isRoot = flow([get(['units', '0', 'type']), isEqual('root')]);
const hasNoChild = flow([get(['units', '0', 'units']), size, isEqual(0)]);
const isRootEmpty = units => isRoot(units) && hasNoChild(units);

function* getServices(): Saga<void> {
	let units = yield* getUnits(actions.fetchUnit([ROOT]));

	if (units && isRootEmpty(units)) {
		logger.debug('Domain is not initialised');
		yield* initDomain(units);
		units = yield* getUnits(actions.fetchUnit([ROOT]));
	}

	const current = yield select(selectTree);
	const strippedNodes = getStrippedNodes(current);

	logger.debug('Requesting Nodes:', strippedNodes);
	units = yield* getUnits(actions.fetchUnit(strippedNodes));
}

const batch = []; // TODO: remove duplicates
function* handleUnitRequest({payload}: RequestUnitAction): Saga<void> {
	batch.push(...payload);

	yield delay(SECOND);

	// TODO: Decide whether to check for unit is stripped
	const units = batch.filter(get('stripped'));

	if (units.length) {
		logger.log('Requesting batch of %d', batch.length);
		yield put(actions.fetchUnit(units));
	} else {
		logger.log('Nothing to request, bailing out');
	}
	batch.length = 0;
}

// const parseParent = uid => uid.replace(/\/[^/]*$/, '');
// const collapseUid = flow([split('/'), take(3), join('/')]);
// const extractUids = flow([map(get('uid')), map(collapseUid), uniq]);
// const extractParents = flow([
// 	map(get('uid')),
// 	// map(parseParent),
// 	map(collapseUid),
// 	uniq,
// ]);
// const extractParents = flow([map(get(['parent', 'uid'])), uniq]);
const CAM_LEVEL = 9;
const log = tap(logger.log);

function* getUnitUpdates(updated: UnitList, isParent: boolen): Saga<void> {
	// const uids = isParent ? extractUids(units) : extractParents(units);
	const units = yield select(get('units'));
	const reqUnits = flow([
		map(flow([get('path'), take(CAM_LEVEL), get(placeholder, units)])),
		uniqBy('uid'),
	])(updated);

	yield* getUnits(actions.requestUnit(reqUnits));
}
const sanitiseObjUnits = unit => (isObject(unit) ? sanitiseUnit(unit) : unit);
const isAdded = isEqual('added');
const isRemoved = isEqual('removed');
const EMPTY_CHANGESET = {
	added: [],
	changed: [],
	removed: [],
};
function* updateHandler(op, {payload: {units}}): Saga<void> {
	const cleanUnits = map(sanitiseUnit, units);
	const {/* changeset, */ error} = yield* applyUnitChangeset({
		...EMPTY_CHANGESET,
		...{[op]: cleanUnits},
	});

	if (!error) {
		// TODO: server returns {} on removal
		// const {[op]: change} = changeset;
		const hasParent = isAdded(op);

		yield* getUnitUpdates(units, hasParent);
	}
}

function* watcher(): Saga<void> {
	yield takeEvery(actions.getServices, getServices);
	yield takeEvery(actions.fetchUnit, getUnits);
	yield takeLatest(actions.requestUnit, handleUnitRequest);
	yield takeEvery(actions.changeUnit, updateHandler, 'changed');
	yield takeEvery(actions.addUnit, updateHandler, 'added');
	yield takeEvery(actions.rmUnit, updateHandler, 'removed');
}

export default watcher;
