// @flow

import * as React from 'react';
import * as prop from 'utils/props';

import type {UnitType} from 'containers/Units/types';

export default function useUnitCollector(unit: UnitType) {
	const [snapshot, setSnapshot] = React.useState(unit);
	const setUnitProp = React.useCallback(
		(path, item) => setSnapshot(current => prop.set(current, path, item)),
		[]
	);
	const getUnitProp = React.useCallback(
		(path, id) => prop.get(snapshot, path, id),
		[snapshot]
	);
	const getInternal = React.useCallback(() => snapshot, [snapshot]);
	const getSnapshot = React.useCallback(() => snapshot, [snapshot]);
	const resetSnapshot = React.useCallback(() => setSnapshot(unit), [unit]);

	return {setUnitProp, getUnitProp, getInternal, getSnapshot, resetSnapshot};
}
