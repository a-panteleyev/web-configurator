// @flow

import * as unitType from './unitTypes';
import {unit} from './actions';

export type Path = Array<string>;
export type Uid = string;
export type Pid = string;
export type Type = string;
export type Constraint = {
	properties: Array<Property>, // eslint-disable-line
	value_string: string,
};

export type EnumConstraint = {
	items: Array<Constraint>,
};

export type RangeConstraint = {
	min_int: number,
	max_int: number,
};

export type Property = {
	id: Pid,
	name: string,
	type: 'string' | 'int32' | 'bool',
	value_string?: string,
	value_int32?: number,
	value_bool?: boolean,
	enum_constraint?: EnumConstraint,
	range_constraint?: RangeConstraint,
};

export type Factory = {
	type: string,
	properties: Array<Property>,
};

export type Unit = {
	type: Type,
	display_id: string,
	uid: Uid,
	properties: Array<Property>,
	status?: string,
	stripped?: boolean,
	factory?: Array<Factory>,
	units?: Array<Unit>,
};

export type UnitType = $Values<unitType>;

export type Changeset = {
	added?: Array<Unit>,
	changed?: Array<Unit>,
	removed?: Array<Unit>,
};

export type Changes = {
	added: Array<Uid>,
	changed: Array<Uid>,
	removed: Array<Uid>,
};

export type UnitIndex = Map<Uid, Unit>;
export type UnitList = Array<Unit | empty>;

/** Action Types */
export type RequestParams = {
	//	uids: Array<string> | string | void,
	units: Array<Unit>,
};

export type UpdateParams = {
	units: UnitList,
};

export type ChangeParams = {
	changeset: Changeset,
};

export type ChildrenParams = {
	parent: string,
	units: UnitList,
};

export type UnitRequestPayload = {
	parent: string,
};

export type UnitChangedPayload = {
	changeset: Changeset,
};

export type UnitUpdatePayload = {
	units: UnitList,
};

type ToString = () => string;

type UnitAction<T, P> = {
	(T): {type: P, payload: T},
	toString: ToString,
};

export type RequestUnitAction = UnitAction<RequestParams, typeof unit.REQUEST>;
export type FetchUnitAction = UnitAction<RequestParams, typeof unit.FETCH>;
export type ChangeUnitAction = UnitAction<UpdateParams, typeof unit.CHANGE>;
export type UpdateUnitAction = UnitAction<UpdateParams, typeof unit.UPDATE>;
export type AddUnitAction = UnitAction<ChildrenParams, typeof unit.ADD>;
export type RmUnitAction = UnitAction<ChildrenParams, typeof unit.REMOVE>;
