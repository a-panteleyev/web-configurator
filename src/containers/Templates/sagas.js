// @ flow

import {takeEvery, put, call, select} from 'redux-saga/effects';
import keyBy from 'lodash/fp/keyBy';

import logger from 'utils/logger';
import * as api from 'api/templates';
import type {Saga} from 'redux-saga';
import * as unitsActions from 'containers/Units/actions';
import {getUnitsByUid} from 'containers/Units/utils';
import type {Unit} from 'containers/Units/types';
import * as actions from './actions';

function* getTemplates() {
	const {items, error} = yield call(api.getTemplates);

	if (error) {
		logger.error('Templates list failed to fetch');
	}

	if (items) {
		yield put(actions.listTmpl(keyBy(({body: {id}}) => id, items)));
	}
}

function* updateTemplates(changeset) {
	const {changes, error} = yield call(api.postTmplChanges, changeset);

	if (error) {
		logger.error('Template change failed');
	}
	logger.debug(changes);
}

// TODO: combine similar methods to a factory
function* addTemplates({payload: template}) {
	yield* updateTemplates({
		created: [template],
		modified: [],
		removed: [],
	});
}

function* modifyTemplates({payload: template}) {
	yield* updateTemplates({
		created: [],
		modified: {items: [template]},
		removed: [],
	});
}

function* rmTemplates({payload: templateIds}) {
	yield* updateTemplates({
		created: [],
		modified: [],
		removed: templateIds,
	});
}

function* assignTemplates({payload: {uids, templateIds}}) {
	const units = yield select(({units}) => getUnitsByUid(uids, units));
	const changeset = {
		items: units.map(({uid, template_ids = []}: Unit) => ({
			unit_id: uid,
			template_ids: [...template_ids, ...templateIds],
		})),
	};

	const {error} = yield call(api.postTmplAssign, changeset);

	if (error) {
		logger.error('Templates change failed');
	} else {
		yield put(unitsActions.fetchUnit({uids}));
	}
}

function* sagas(): Saga<void> {
	yield takeEvery(actions.getTmpl, getTemplates);
	yield takeEvery(actions.addTmpl, addTemplates);
	yield takeEvery(actions.modTmpl, modifyTemplates);
	yield takeEvery(actions.rmTmpl, rmTemplates);
	yield takeEvery(actions.assignTmpl, assignTemplates);
}

export default sagas;
