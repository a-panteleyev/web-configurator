// @flow

export const REQUEST = 'template/REQUEST';
export const ADD = 'template/ADD';
export const MODIFY = 'template/MODIFY';
export const REMOVE = 'template/REMOVE';
export const LIST = 'template/LIST';
export const ASSIGN = 'template/ASSIGN';
