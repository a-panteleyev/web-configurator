// @flow

import {createAction} from 'redux-act';
import * as C from './constants';
import * as T from './types';

export const getTmpl: T.GetTmplAction = createAction(C.REQUEST);
export const addTmpl: T.AddTmplAction = createAction(C.ADD);
export const modTmpl: T.SetTmplAction = createAction(C.MODIFY);
export const rmTmpl: T.RmTmplAction = createAction(C.REMOVE);
export const listTmpl: T.ListTmplAction = createAction(C.LIST);
export const assignTmpl: T.AssignTmplAction = createAction(C.ASSIGN);
