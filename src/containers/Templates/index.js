import * as React from 'react';
import {compose, bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from './actions';

const mapActionsToProps = dispatch => bindActionCreators(actions, dispatch);

export const withTemplates =
	connect(({templates}) => ({templates}), mapActionsToProps);

export const SyncTemplates = withTemplates(
	class Templates extends React.Component {
		componentDidMount() {
			this.props.getTmpl();
		}
		render() {
			return null;
		}
	}
);
