// @flow

import type {Uid, Property, Type} from 'containers/Unit/types';
import * as C from './constants';

export type Unit = {
	uid: Uid,
	type: Type,
	properties: Array<Property>,
	opaque_params: Array<Property>,
	units: Array<Unit>,
};
export type Tid = string;
export type Template = {
	body: {
		id: Tid,
		name: string,
		unit: Unit,
	},
	etag?: string,
};

export type TemplateIndex = {
	[string]: Template,
};

type Changeset = {
	added: Array<Template>,
	modified: Array<Template>,
	removed: Array<Tid>,
};
// type Assignset = {
// 	items: Array<{
// 		template_ids: Array<Tid>,
// 		unit_id: Uid,
// 	}>,
// };
type Assignset = {
	templateIds: Array<Tid>,
	uids: Array<Uid>,
};
type ToString = () => string;
type TemplateAction<T, P> = {
	(P): {type: T, payload: P},
	toString: ToString,
};

export type AddTmplAction = TemplateAction<typeof C.ADD, Changeset>;
export type RmTmplAction = TemplateAction<typeof C.REMOVE, Changeset>;
export type SetTmplAction = TemplateAction<typeof C.MODIFY, Changeset>;
export type GetTmplAction = TemplateAction<typeof C.REQUEST, empty>;
export type AssignTmplAction = TemplateAction<typeof C.ASSIGN, Assignset>;
export type ListTmplAction = TemplateAction<typeof C.LIST, empty>;
