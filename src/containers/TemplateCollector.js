// @flow

import * as React from 'react';
import assign from 'lodash/fp/assign';
import flow from 'lodash/fp/flow';
import flatMap from 'lodash/fp/flatMap';
import initial from 'lodash/fp/initial';
import last from 'lodash/fp/last';
import reduce from 'lodash/fp/reduce';
import sortBy from 'lodash/fp/sortBy';
import update from 'lodash/fp/update';

import * as prop from 'utils/props';
import {cloneStructure, resolveUnit, sanitiseUnit} from 'utils/units';
import logger from 'utils/logger';
import type {Unit} from 'containers/Unit/type';

const makeStrPath = path => path.join('\0');
const revertStrPath = pathStr => pathStr.split('\0');
const sortByLen = sortBy(path => -path.length);

type Path = Array<string>;

export default function useTmplCollector() {
	const [template, setTemplate] = React.useState(new Set());
	const toggleTmpl = (path: Path, wantSet: boolean) => {
		const pathStr = makeStrPath(path);

		if (wantSet) {
			template.add(pathStr);
		} else {
			template.delete(pathStr);
		}
		setTemplate(new Set(template));
		logger.log(template);
	};
	const inTmpl = (path: Path) => template.has(makeStrPath(path));
	const getPropTmpl = (unit: Unit) => {
		const blankStructure = cloneStructure(unit);

		return reduce(
			(tmpl, pathStr) => {
				const pathArr = revertStrPath(pathStr);
				const path = initial(pathArr);
				const id = last(pathArr);

				return prop.set(tmpl, path, prop.get(unit, path, id));
			},
			blankStructure,
			Array.from(template)
		);
	};
	const getUnitTmpl = (unit: Unit) =>
		// TODO: Fix absence of type for intermediary unit
		sanitiseUnit(
			reduce(
				(tmpl, pathStr) => {
					const paths = revertStrPath(pathStr);
					const end = paths.length - 1;
					let coll = tmpl;

					for (let i = 0; i < paths.legth; i += 1) {
						const {
							resolv,
							curUnit: {uid, type, properties},
						} = resolveUnit(paths, unit);
						const tempPart = {uid, type, properties: []};

						if (i === end) {
							tempPart.properties = properties;
						}

						coll = update(resolv, assign(tempPart), coll);
						paths.pop();
					}

					return coll;
				},
				{uid: unit.uid, type: unit.type, properties: [], units: []},
				sortByLen(Array.from(template))
			)
		);
	const resetTmpl = () => setTemplate(new Set());
	const getInternal = () => template;

	return {
		toggleTmpl,
		inTmpl,
		resetTmpl,
		getTmpl: getUnitTmpl,
		getInternal,
	};
}
