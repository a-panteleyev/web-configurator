// @flow
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from './actions';

const mapStateToProps = ({workbench}) => ({
	wbUid: workbench.get('uid'),
	wbExtensions: workbench.get('extensions'),
	wbUnits: workbench.get('units'),
});
const mapActionsToProps = (dispatch: Function) =>
	bindActionCreators(actions, dispatch);

export const withWorkbench = connect(
	mapStateToProps,
	mapActionsToProps
);
export default {withWorkbench};
