// @flow
import {createAction} from 'redux-act';

import type {
	SetUnitAction,
	AddExtUnitAction,
	RmExtUnitAction,
	BuildUnitAction,
	AddUnitAction,
	RmUnitAction,
	SetUnitPropAction,
	RmUnitPropAction,
} from './types';

export const setUnit: SetUnitAction = createAction('workbench/SET_UNIT');
export const addExtUnit: AddExtUnitAction = createAction('workbench/ADD_EXT_UNIT');
export const rmExtUnit: RmExtUnitAction = createAction('workbench/RM_EXT_UNIT');
export const buildUnit: BuildUnitAction = createAction('workbench/BUILD_UNIT');// this
export const addUnit: AddUnitAction = createAction('workbench/ADD_UNIT');// 			or that?
export const rmUnit: RmUnitAction = createAction('workbench/RM_UNIT');
export const setUnitProp: SetUnitPropAction = createAction('workbench/SET_UNIT_PROP');
export const rmUnitProp: RmUnitPropAction = createAction('workbench/RM_UNIT_PROP');
