// @flow

import {createReducer} from 'redux-act';
import * as actions from './actions';

const initialState: string = '';

export default createReducer(
	{
		[actions.searchSet]: (_, str) => str,
		[actions.searchReset]: () => initialState,
	},
	initialState
);
