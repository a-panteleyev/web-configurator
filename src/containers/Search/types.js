import * as c from './constants';

export type setAction = string => {type: c.SET, payload: string};
export type resetAction = string => {type: c.RESET};
export type searchAction = setAction | resetAction;
