// @flow

import {createAction} from 'redux-act';
import * as c from './constants';
import type {setAction, resetAction} from './types';

export const searchSet: setAction = createAction(c.SET);
export const searchReset: resetAction = createAction(c.RESET);
