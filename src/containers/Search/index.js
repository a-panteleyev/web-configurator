import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from './actions';

const mapActionsToProps = dispatch => bindActionCreators(actions, dispatch);
export const withSearch = connect(
	({search}) => ({search}),
	mapActionsToProps
);

export default {withSearch};
