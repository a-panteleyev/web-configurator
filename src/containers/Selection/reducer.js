import {createReducer} from 'redux-act';

import * as actions from './actions';

export type SelectionType = Set<string>;

const initialState: SelectionType = new Set();

export default createReducer(
	{
		[actions.select]: (state, id) => (state.add(id), new Set(state)),
		[actions.deselect]: (state, id) => (state.delete(id), new Set(state)),
		[actions.clear]: state => (state.clear(), new Set(state)),
	},
	initialState
);
