// @flow
import {all, fork} from 'redux-saga/effects';
import appSaga from 'containers/App/sagas';
import unitSaga from 'containers/Units/sagas';
import tmplSaga from 'containers/Templates/sagas';

export default function* () {
	yield all([
		fork(appSaga),
		fork(unitSaga),
		fork(tmplSaga),
	]);
};
