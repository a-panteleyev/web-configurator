// @flow

import * as React from 'react';
import cx from 'classnames';
import {withStyles} from '@material-ui/core/styles';

type HexProps = {
	classes: Object,
	className: string,
	children: React.Node,
}

const hexStyles = {
	hexPlaceholder: {
		position: 'relative',
		visibility: 'hidden',
		outline: '1px solid transparent',
		width: '20%',
		pointerEvents: 'none',
		'&::after': {
			content: '""',
			display: 'block',
			paddingBottom: '86.602%',
		},
		'&:nth-child(9n+6)': {
			marginLeft: '10%',
		},
		/*
		'& *': {
			position: 'absolute',
			visibility: 'visible',
			outline: '1px solid transparent',
		}
		*/
	},
	hexSkew: {
		position: 'absolute',
		width: '96%',
		paddingBottom: '110.851%',
		margin: '0 2%',
		overflow: 'hidden',
		visibility: 'hidden',
		outline: '1px solid transparent',
		transform: 'rotateZ(-60deg) skewY(30deg)',
		visibility: 'visible',
		outline: '1px solid transparent',
	},
	hexUnskew: {
		width: '100%',
		height: '100%',
		overflow: 'hidden',
		transform: 'skewY(-30deg) rotateZ(60deg)',
		pointerEvents: 'all',
		position: 'absolute',
		visibility: 'visible',
		outline: '1px solid transparent',
	},
};
const HexUs = ({children, className, classes, onClick}: HexProps) => (
	<li className={cx(className, classes.hexPlaceholder)} onClick={onClick}>
		<div className={classes.hexSkew}>
			<div className={classes.hexUnskew}>
				{children}
			</div>
		</div>
	</li>
);
export const Hex = withStyles(hexStyles)(HexUs);

const hexGridStyles = theme => ({
	root: {
		display: 'flex',
		backgroundColor: theme.palette.background.default,
		margin: 0,
		paddingTop: theme.spacing.unit * 5,
		paddingBottom: '4.6%',
		// width: '80%',
		height: '100%',
		overflow: 'hidden',
		flexWrap: 'wrap',
		listStyleType: 'none',
	}
});
const HexGridUs = ({children, classes, ...props}: HexProps): React.Node => (
	<ul className={classes.root} {...props}>
		{children}
	</ul>
);
export const HexGrid = withStyles(hexGridStyles)(HexGridUs);

export default HexGrid;

