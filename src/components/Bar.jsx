// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Fade from '@material-ui/core/Fade';
import ClearIcon from '@material-ui/icons/Clear';
import withStyles from '@material-ui/core/styles/withStyles';

import {withSelection, SelectionType} from 'containers/Selection';
import DeleteUnits from 'components/DeleteUnits';
import TemplateAssign from 'components/TemplateAssign';

const styles = theme => ({
	root: {
		transition: theme.transitions.easing.easingInOut,
		opacity: 1,
	},
	hidden: {
		opacity: 0,
		pointerEvents: 'none',
	},
});

type Props = {
	classes: Object,
	selection: SelectionType,
	clear: Function,
};

const Bar = ({classes, selection, clear}: Props) => {
	const hasSelected = selection.size > 0;
	const uids = [...selection];

	return (
		<Fade in={hasSelected}>
			<AppBar
				className={hasSelected ? classes.root : classes.hidden}
				color="default"
			>
				<Toolbar>
					<Typography variant="title" color="textSecondary">
						{/* <FormattedMessage id="selection.size" /> */}
						{selection.size}
					</Typography>
					<Button onClick={clear}>
						<FormattedMessage id="selection.clear" />
						<ClearIcon />
					</Button>
					<DeleteUnits uids={uids} onDone={clear} />
					<TemplateAssign uids={uids} onDone={clear} />
				</Toolbar>
			</AppBar>
		</Fade>
	);
};

export default compose(
	withStyles(styles),
	withSelection
)(Bar);
