import React, {cloneElement, Component} from 'react';
import {FormattedMessage} from 'react-intl';
import cn from 'classnames';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import ExpandIcon from '@material-ui/icons/ExpandMore';

const FULL_HEIGHT = '100vh';
const styles = theme => ({
	container: {
		transitionDelay: ['0s', `${theme.transitions.duration.short}ms`],
		transition: [
			['max-height', `${theme.transitions.duration.standard}ms`],
			['opacity', `${theme.transitions.duration.complex}ms`],
		],
	},
	collapsed: {
		maxHeight: 0,
	},
	expanded: {
		maxHeight: FULL_HEIGHT,
		opacity: 1,
	},
});

type Props = {
	expanded: boolean,
	children: Rect.Node,
	classes: Object,
};
function UnstyledExpansion({expanded, children, classes}: Props) {
	const [filled, setFilled] = React.useState(expanded);
	const updateVis5ty = React.useCallback(() => {
		if (!expanded) {
			setFilled(false);
		}
	}, [null]);

	return (
		<div
			className={cn(classes.container, classes.collapsed, {
				[classes.expanded]: expanded,
			})}
			onTransitionEnd={updateVis5ty}
		>
			{filled ? children : null}
		</div>
	);
}

export const Expansion = withStyles(styles)(UnstyledExpansion);

function UnstyledHideExpansion(props: Props) {
	const {expanded, children, classes} = props;

	return (
		<div className={cn(classes.collapsed, {[classes.expanded]: expanded})}>
			{children}
		</div>
	);
}

export const HideExpansion = withStyles(styles)(UnstyledHideExpansion);

//
// class Expand extends Component {
// 	constructor(props) {
// 		super(props);
// 		this.state = {
// 			isExpanded: false,
// 		};
// 		this.toggleExpand = () =>
// 			this.setState({isExpanded: !this.state.isExpanded});
// 	}
//
// 	render() {
// 		const {isExpanded} = this.state;
// 		const {children, label, theme} = this.props;
// 		const {
// 			palette: {textColor},
// 		} = theme;
// 		const iconStyle = {
// 			transform: isExpanded && 'rotate(180deg)',
// 		};
// 		const labelEl = label || <FormattedMessage id="more" />;
// 		const Top = (
// 			<div styleName="top">
// 				<Typography variant="caption" styleName="label">
// 					{labelEl}
// 				</Typography>
// 				<IconButton styleName="top__expand" style={iconStyle}>
// 					<ExpandIcon color={textColor} />
// 				</IconButton>
// 			</div>
// 		);
//
// 		return (
// 			<div>
// 				{cloneElement(Top, {onClick: this.toggleExpand})}
// 				<HideExpansion expanded={isExpanded}>
// 					{children}
// 				</HideExpansion>
// 			</div>
// 		);
// 	}
// }
//
// export default compose(
// 	withTheme(),
// 	CSSModules(styles)
// )(Expand);
