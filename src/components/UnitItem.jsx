// @flow

import * as React from 'react';
import {FormattedMessage} from 'react-intl';
import {compose} from 'redux';
import cn from 'classnames';
import noop from 'lodash/fp/noop';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grid from '@material-ui/core/Grid';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import Popover from '@material-ui/core/Popover';
import TextField from '@material-ui/core/TextField';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import UknownIcon from '@material-ui/icons/HelpOutline';
import ActiveIcon from '@material-ui/icons/PlayCircleOutline';
import InactiveIcon from '@material-ui/icons/PauseCircleOutline';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Edit from '@material-ui/icons/Edit';
import {withStyles} from '@material-ui/core/styles';
import * as colors from '@material-ui/core/colors';

import * as props from 'utils/props';
import {getCustomTmplId} from 'utils/uuid';
import logger from 'utils/logger';
import {findProp} from 'utils/props';
import {withUnit, unitType} from 'containers/Units';
import {withTemplates} from 'containers/Templates';
import type {SetTmplAction} from 'containers/Templates/types';
import useTmplCollector from 'containers/TemplateCollector';
import type {
	UnitType,
	Unit as UnitT,
	RequestUnitAction,
} from 'containers/Units/types';
import {Camera} from 'components/Camera';
import ColorPicker from 'components/ColorPicker';
import {Expansion} from 'components/Expand';
import Node from 'components/Node';
import Storage from 'components/Storage';
import Unit from 'components/Unit2';
import PresentUnit from 'components/PresentUnit';
import SubUnitMaker from 'components/SubUnitMaker';
import SubUnitEditor from 'components/SubUnitEditor';

const DISPLAY = 'display';
const EDIT = 'edit';
const SNAPSHOT = 'snapshot';
type Mode = string;

const styles = theme => ({
	card: {
		overflow: 'initial',
		pointer: 'cursor',
		'&:hover $display': {
			visibility: 'initial',
		},
		transition: [
			['margin', `${theme.transitions.duration.standard}ms`],
			['padding', `${theme.transitions.duration.standard}ms`],
		],
		transitionTimingFunction: theme.transitions.easing.easeInOut,
	},
	snapshotBtn: {
		// position: 'absolute',
		marginTop: theme.spacing.unit * 4,
	},
	cardActive: {
		margin: -theme.spacing.unit * 4,
		padding: theme.spacing.unit * 4,
		zIndex: 3,
	},
	container: {
		// position: 'relative',
	},
	active: {
		color: theme.palette.primary.main,
	},
	empty: {
		visibility: 'initial',
	},
	snapshotSubmit: {
		marginTop: theme.spacing.unit * 4,
	},
	tmplDialog: {
		margin: theme.spacing.unit * 4,
	},
	display: {
		visibility: 'hidden',
	},
	displayTab: {
		width: 0,
		minWidth: 0,
	},
	tabIcon: {
		minWidth: theme.spacing.unit * 4,
	},
	title: {
		color: theme.palette.text.hint,
		transform: 'scale(2)',
		transformOrigin: '0% 50%',
	},
});

const getComponent = (type: UnitType): React.ComponentType<any> | string => {
	switch (type) {
		case unitType.CAM:
			return Camera;
		case unitType.NODE:
			return Node;
		case unitType.STORAGE:
			return Storage;
		default:
			return 'div';
	}
};

function getStatusIcon(status) {
	switch (status) {
		case 'UNIT_STATUS_ACTIVE':
			return ActiveIcon;
		case 'UNIT_STATUS_INACTIVE':
			return InactiveIcon;
		default:
			return UknownIcon;
	}
}

function ClickAwayWrap({
	children,
	enabled,
	...rest
}: {
	children: React.Node,
	enabled: boolean,
	onClickAway: Function,
}) {
	let output = children;

	if (enabled && false) {
		output = (
			<ClickAwayListener {...rest}>
				{children}
			</ClickAwayListener>
);
	}

	return output;
}

const findName = findProp('display_name');
const center = {
	vertical: 'center',
	horizontal: 'center',
};

type Props = {
	classes: Object,
	className: string,
	type: UnitType,
	unit: UnitT,
	requestUnit: RequestUnitAction,
	addTmpl: SetTmplAction,
};

function UnitItem({
	type,
	unit,
	addTmpl,
	requestUnit,
	isVisible,
	classes,
	className,
}: Props) {
	// const Content = getComponent(type);
	const [mode, setMode] = React.useState(DISPLAY);
	const [subfactory, setSubfactory] = React.useState(null);
	const resetSubfactory = React.useCallback(() => setSubfactory(null), []);
	const [editUnit, setEdit] = React.useState(null);
	const resetEdit = React.useCallback(() => setEdit(null), []);
	const template = useTmplCollector();
	const changeMode = React.useCallback(
		(value: Mode) => {
			let newMode = value;

			if (mode === value) {
				newMode = DISPLAY;
			}
			setMode(newMode);
			template.resetTmpl();
			resetSubfactory();
			logger.log('UnitItem mode:', newMode);
		},
		[mode]
	);
	const handleChange = React.useCallback(
		(_, value: Mode) => changeMode(value),
		[mode]
	);
	const resetMode = React.useCallback(() => changeMode(DISPLAY), []);

	/** * Snapshot ** */
	const submitSnapshot = React.useCallback(
		(e: SyntheticEvent<HTMLFormElement>) => {
			e.preventDefault();
			const tmplUnit = template.getTmpl(unit);
			const form = e.currentTarget;
			const tmpl = {
				id: getCustomTmplId(Math.random().toString()),
				name: form.templateName.value,
				unit: {
					...tmplUnit,
					opaque_params: [
						{id: 'color', type: 'string', value_string: form.color.value},
					],
				},
			};

			logger.log('submit template', tmpl);
			addTmpl(tmpl);
			template.resetTmpl();
			setMode(DISPLAY);
		},
		[template.getInternal()]
	);
	const displayMode = mode === DISPLAY;
	const editMode = mode === EDIT && !unit.stripped;
	const snapshotMode = mode === SNAPSHOT && !unit.stripped;
	const cardClasses = cn(className, classes.card, {
		[classes.cardActive]: !displayMode,
		[classes.cardStripped]: unit.stripped,
	});
	const snapshotClasses = cn({
		[classes.display]: displayMode,
		[classes.active]: snapshotMode,
	});
	const editClasses = cn({
		[classes.display]: displayMode,
		[classes.active]: editMode,
	});
	const containerClasses = cn(classes.container, {
		[classes.containerSide]: Boolean(subfactory),
	});
	const elevation = displayMode ? 2 : 8;

	/** * Request complete unit ** */
	const [pending, setPending] = React.useState(false);
	const StatusIcon = getStatusIcon(unit.status);
	const req = React.useCallback(() => {
		// TODO: or handle it in list?
		if (unit.stripped) {
			requestUnit([unit]);
			setPending(true);
		}
	}, [unit]);

	React.useEffect(() => {
		if (!unit.stripped) {
			setPending(false);
		}

		return noop;
	});

	return (
		<ClickAwayWrap onClickAway={resetMode} enabled={!displayMode}>
			<Card className={cardClasses} elevation={elevation}>
				<CardHeader
					avatar={
						pending ? (
							<CircularProgress size={24} variant="indeterminate" />
						) : (
							<StatusIcon />
						)
					}
					action={(
						<Tabs onChange={handleChange} value={mode}>
							<Tab classes={{root: classes.displayTab}} value={DISPLAY} />
							<Tab
								centerRipple
								classes={{root: classes.tabIcon}}
								className={editClasses}
								value={EDIT}
								icon={<Edit />}
							/>
							<Tab
								centerRipple
								classes={{root: classes.tabIcon}}
								className={snapshotClasses}
								value={SNAPSHOT}
								icon={<PhotoCamera />}
							/>
						</Tabs>
)}
					classes={{title: classes.title}}
					title={unit.stripped && `${unit.display_id}`}
					onClick={req}
				/>
				<div className={containerClasses}>
					{!unit.stripped && (
						<PresentUnit
							{...{
								unit,
								snapshotMode,
								editMode,
								setSubfactory,
								setEdit,
								...template,
							}}
						/>
					)}
					{/* }
			<Popover
				anchorEl={anchorEl}
				anchorOrigin={center}
				transformOrigin={center}
				open={Boolean(anchorEl)}
				onClose={close}
			>
				<Unit onClose={close} {...{unit}} />
			</Popover>
			{ */}
					<SubUnitMaker factory={subfactory} onCancel={resetSubfactory} />
					<SubUnitEditor
						unit={editUnit}
						onCancel={resetEdit}
						{...{requestUnit, root: unit}}
					/>
				</div>
				{/* TODO: Extract to separate Component */}
				<Collapse in={snapshotMode}>
					<form onSubmit={submitSnapshot}>
						<Grid alignItems="center" container direction="column">
							<TextField
								label={<FormattedMessage id="template.name" />}
								required
								margin="normal"
								name="templateName"
								fullWidth
							/>
							<ColorPicker />
							<Button
								classes={{root: classes.snapshotSubmit}}
								type="submit"
								color="primary"
							>
								<FormattedMessage id="unit.snapshot" />
							</Button>
						</Grid>
					</form>
				</Collapse>
			</Card>
		</ClickAwayWrap>
	);
}

export default compose(
	withUnit,
	withStyles(styles),
	withTemplates
)(UnitItem);
