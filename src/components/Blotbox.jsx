// @flow

import * as React from 'react';
import cn from 'classnames';
import noop from 'lodash/fp/noop';
import Backdrop from '@material-ui/core/Backdrop';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import {MuiThemeProvider, withStyles} from '@material-ui/core/styles';
import {darken} from '@material-ui/core/styles/colorManipulator';
import {darkTheme} from 'theme';
import ContentAdd from '@material-ui/icons/Add';
import Close from '@material-ui/icons/Close';

const AUTO = 'auto';
const VISIBLE = 'visible';
const HIDDEN = 'hidden';
const NONE = 'none';
const ZERO = '0ms';

const styles = theme => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		margin: -theme.spacing.unit * 2,
		padding: theme.spacing.unit * 2,
		position: 'absolute',
		transition: [
			// TODO: double check that units are required
			['box-shadow', `${theme.transitions.duration.standard}ms`],
			['max-height', `${theme.transitions.duration.complex}ms`],
			['max-width', `${theme.transitions.duration.complex}ms`],
			['margin', `${theme.transitions.duration.complex}ms`],
		],
		transitionDelay: [theme.transitions.duration.complex, ZERO, ZERO, ZERO],
		overflow: 'hidden',
		...theme.shape,
	},
	actionButton: {
		opacity: 0,
		position: 'absolute',
		left: '50%',
		top: '50%',
		transform: 'translate(-50%, -50%)',
		transition: [
			['opacity', `${theme.transitions.duration.complex}ms`],
			['box-shadow', `${theme.transitions.duration.standard}ms`],
		],
		transitionDelay: [
			`${theme.transitions.duration.standard}ms`,
			`${theme.transitions.duration.complex}ms`,
		],
		'&__icon': {
			transition: [['opacity', `${theme.transitions.duration.complex}ms`]],
			transitionDelay: `${theme.transitions.duration.complex}ms`,
		},
	},
	ink: {
		borderRadius: '50%',
		position: 'absolute',
		transform: 'scale(30)',
		transition: `${theme.transitions.duration.complex}ms`,

		display: 'inline-block',
		margin: 'auto',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
	},
	content: {
		opacity: 0,
		padding: [
			[
				theme.spacing.unit * 2,
				theme.spacing.unit * 5,
				0,
				theme.spacing.unit * 5,
			],
		],
		overflowY: 'auto',
		overflowX: 'hidden',
		transition: [['opacity', `${theme.transitions.duration.standard}ms`]],
		transitionDelay: `${theme.transitions.duration.complex}ms`,
		maxHeight: '80vh',
		zIndex: 'inherit',
	},
	closeButton: {
		pointerEvents: 'none',
		position: 'absolute',
		top: 0,
		right: 0,
		transform: [['translateX(100%)', 'rotate(90deg)']],
		transition: [['transform', `${theme.transitions.duration.standard}ms`]],
		transitionDelay: `${theme.transitions.duration.standard}ms`,
		zIndex: 'inherit',
	},
	backdrop: {
		zIndex: 1,
		'&__invisi': {
			zIndex: -1,
		},
	},
});

type Props = {
	color: string,
	children: React.Element<*>,
	className: string,
	active: Function,
	icon: React.Element<*>,
	mini: boolean,
	style: Object,
	theme: Object,
	backdrop: boolean,
	onClose: Function,
	stopEventPropagation: boolean,
};

type State = {
	open: boolean,
	isShown: boolean,
};

class Blotbox extends React.Component<Props, State> {
	theme = darkTheme;

	static defaultProps = {
		active: noop,
		onClose: noop,
	};

	state = {
		open: false,
		isShown: false,
	};

	open = (e: SyntheticMouseEvent<HTMLElement>) => {
		const {active, stopEventPropagation} = this.props;

		if (stopEventPropagation) {
			e.stopPropagation();
		}
		this.setState({open: true, isShown: true}, () => active(true));
	};

	close = () => {
		const {onClose} = this.props;

		this.setState({open: false}, onClose);
	};

	transitionComplete = e => {
		if (e.target !== e.currentTarget || e.propertyName !== 'opacity') {
			return;
		}
		const {active} = this.props;

		this.setState(
			({open}) => ({isShown: open}),
			() => {
				const {open} = this.state;

				active(open);
			}
		);
	};

	render() {
		const {
			backdrop,
			children,
			classes,
			color,
			className,
			icon,
			mini,
			style,
			theme,
		} = this.props;

		const {open, isShown} = this.state;

		const openReady = open && isShown;
		const bColor = color || theme.palette.secondary.main;
		const size = mini ? 40 : 56;
		const inkColor = open ? theme.palette.background.paper : bColor;
		const containerStyle = {
			...style,
			boxShadow: open ? theme.shadows[13] : NONE,
			minHeight: size,
			minWidth: size,
			maxHeight: open ? window.innerWidth : size,
			maxWidth: open ? window.innerHeight : size,
			margin: open && 0,
			transitionDelay: !open && ZERO,
		};
		const backdropStyle = {
			zIndex: open ? 0 : -1,
		};
		const buttonStyle = {
			opacity: !open && 1,
			zIndex: open && -1,
		};
		const iconStyle = {
			opacity: !open && 1,
			transitionDelay: open && ZERO,
		};
		const inkStyle = {
			background: inkColor,
			height: size,
			width: size,
			transform: !open && NONE,
			transitionTimingFunction: open
				? theme.transitions.easing.easeIn
				: theme.transitions.easing.easeOut,
		};
		const contentStyle = {
			opacity: openReady && 1,
			transitionDelay: !open && ZERO,
			padding: !open && 0,
			pointerEvents: !open && NONE,
			zIndex: open && 0,
		};
		const closeButtonStyle = {
			pointerEvents: openReady && AUTO,
			transform: openReady ? NONE : 'translateX(100%) rotate(90deg)',
			transitionProperty: 'all',
			transitionDuration: theme.transitions.duration.standard,
			transitionDelay: openReady ? theme.transitions.duration.standard : ZERO,
			opacity: open ? 1 : 0,
		};

		/* <MuiThemeProvider theme={this.theme}> */
		return (
			<>
				{open && <Backdrop open={open} style={{zIndex: 0}} />}
				<ClickAwayListener onClickAway={this.close}>
					<div className={cn(classes.root, className)} style={containerStyle}>
						<div className={classes.ink} style={inkStyle} />
						{/* }
					{open && closeOnClickOutside && (
						<div  onClick={this.close} /> // TODO: backdrop
					)}
					{ */}
						<div className={classes.content} style={contentStyle}>
							{isShown
								? React.cloneElement(children, {
										onCancel: this.close,
								  })
								: null}
						</div>
						<IconButton
							className={classes.closeButton}
							style={closeButtonStyle}
							onClick={this.close}
						>
							<Close />
						</IconButton>
						<Fab
							color="secondary"
							className={classes.actionButton}
							style={buttonStyle}
							onClick={this.open}
							onTransitionEnd={this.transitionComplete}
						>
							{React.cloneElement(icon || <ContentAdd />, {
								className: classes.actionButton__icon,
								style: iconStyle,
							})}
						</Fab>
					</div>
				</ClickAwayListener>
			</>
		);
	}
}

export default withStyles(styles, {withTheme: true})(Blotbox);
