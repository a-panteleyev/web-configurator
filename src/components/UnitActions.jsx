// @flow

import * as React from 'react';
import Popper from '@material-ui/core/Popper';

type Props = {
	children: React.Node,
};
export default function Actions(props: Props) {
	return (
		<React.Fragment onMouseMove={console.log}>
			{props.children}
		</React.Fragment>
	);
}
