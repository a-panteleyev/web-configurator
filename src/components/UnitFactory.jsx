import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import filter from 'lodash/fp/filter';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import noop from 'lodash/fp/noop';
import isEqual from 'lodash/fp/isEqual';
import sample from 'lodash/fp/sample';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import AddIcon from '@material-ui/icons/Add';
import {withStyles} from '@material-ui/core/styles';

import logger from 'utils/logger';

import {unitType, withUnit, PARENT_FOR} from 'containers/Units';
import UnitMaker, {withUnitCollector} from 'components/UnitMaker';
import useUnitCollector from 'containers/Units/UnitCollector';
import useTmplCollector from 'containers/TemplateCollector';
// import withTmplCollector from 'containers/TemplateCollector';
import type {AddUnitAction} from 'containers/Units/types';
import {selectType} from 'containers/Units/utils';

// TODO: UnitFactory <-> UnitMaker

const styles = theme => ({
	root: {
		width: theme.spacing.unit * 30,
	},
	divider: {
		width: '200%',
		margin: theme.spacing.unit * 2,
	},
	select: {},
});

type Props = {
	classes: Object,
	units: Array<Unit>,
	unit: Unit,
	type: string,
	addUnit: AddUnitAction,
	getDiff: Function,
	onCancel: Function,
};

function UnitFactory({
	classes,
	type,
	addUnit,
	units,
	onCancel,
	...rest
}: Props) {
	const [parentsState, setParentsState] = React.useState({});
	const snapshot = useUnitCollector({});
	const submitUnit = React.useCallback(
		() => {
			const {properties} = snapshot.getSnapshot();
			const unit = {
				uid: parentsState.parent.uid,
				units: [{type, properties}],
			};

			addUnit({units: [unit]});

			logger.log('submit unit', unit);
		},
		[parentsState, snapshot.getInternal()]
	);
	const submitClose = React.useCallback(() => {
		submitUnit();
		onCancel();
	});
	const changeParent = React.useCallback((e, uid) =>
		setParentsState({
			...parentsState,
			parent: find(flow([get('uid'), isEqual(uid)]))(this.props.parents),
		})
	);
	const template = useTmplCollector();

	React.useEffect(
		() => {
			const {parent} = parentsState;
			const parents = selectType(PARENT_FOR[type])(units);

			if (!parent && parents) {
				setParentsState({
					parent: sample(parents),
					parents,
				});
			}
			return noop;
		},
		[parentsState, units]
	);

	const {parent, parents} = parentsState;
	const factory = flow([
		get('factory'),
		find(flow([get('type'), isEqual(type)])),
	])(parent);

	if (!factory) {
		return null;
	}

	return (
		<div className={classes.root}>
			<Grid container alignItems="center" direction="column">
				<TextField
					className={classes.select}
					fullWidth
					value={parent.uid}
					select
					label={<FormattedMessage id="common.node" />}
					SelectProps={{native: true}}
					onChange={changeParent}
				>
					{parents.map(({uid, display_id}) => (
						<option key={uid} value={uid}>
							{display_id}
						</option>
					))}
				</TextField>
				<Divider classes={{root: classes.divider}} />
				<UnitMaker factory={factory} {...rest} {...snapshot} {...template} />
				<div>
					<Button
						className={classes.submit}
						color="primary"
						onClick={submitClose}
						variant="contained"
					>
						<FormattedMessage id="unit.make" />
					</Button>
					<Button
						className={classes.submit}
						color="primary"
						onClick={submitUnit}
						variant="text"
					>
						<AddIcon />
						<FormattedMessage id="unit.make" />
					</Button>
				</div>
			</Grid>
		</div>
	);
}

export default compose(
	withStyles(styles),
	// withUnitCollector,
	withUnit
	// withTmplCollector /* TODO: required for PropItem, examine */
)(UnitFactory);
