// @flow

import * as React from 'react';
import map from 'lodash/fp/map';
// import Chip from '@material-ui/core/Chip';
import Fab from '@material-ui/core/Fab';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
// import MoreVertIcon from '@material-ui/icons/MoreVert';
import Add from '@material-ui/icons/Add';
import {withStyles} from '@material-ui/core/styles';

import logger from 'utils/logger';

import {withUnit, unitType} from 'containers/Units';
import type {Factory} from 'containers/Units/types';

const creators = {
	[unitType.CAM_STREAM]() {},
	[unitType.CAM_AVDETECT]() {},
	[unitType.CAM_ARCH]() {},
};

const StyledFab = withStyles(theme => ({
	root: {},
}))(Fab);

export default function AddSubUnit({
	factory,
	setSubfactory,
}: {
	factory: Factory,
	setSubfactory: Factory => void,
}) {
	if (!factory) {
		return null;
	}

	const [anchor, setAnchor] = React.useState(null);
	const openMenu = React.useCallback(
		(e: SyntheticMouseEvent<HTMLButtonElement>) => setAnchor(e.target),
		[null]
	);
	const closeMenu = React.useCallback(() => setAnchor(null), [null]);

	return (
		<>
			<StyledFab onClick={openMenu} size="small">
				<Add />
			</StyledFab>
			<Menu anchorEl={anchor} open={Boolean(anchor)} onClose={closeMenu}>
				{map((f: Factory) => {
					const addSubUnit = React.useCallback(() => {
						closeMenu();
						setSubfactory(f);
					}, [f]);

					return (
						<MenuItem key={f.type} onClick={addSubUnit}>
							{f.type}
						</MenuItem>
					);
				}, factory)}
			</Menu>
		</>
	);
}
