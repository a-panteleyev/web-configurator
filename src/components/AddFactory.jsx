// @flow

import * as React from 'react';
import get from 'lodash/fp/get';
import map from 'lodash/fp/map';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Grow from '@material-ui/core/Grow';
import {withStyles} from '@material-ui/core/styles';
import Add from '@material-ui/icons/Add';

import {several} from 'utils/misc';
import {useAnchor} from 'utils/material';

import type {Factory, Unit} from 'containers/Units/types';

const getFactory = get('factory');

function FactoryItem({
	factory,
	setSubfactory,
	resetAnchor,
}: {
	factory: Factory,
	setSubfactory: Function,
	resetAnchor: Function,
}) {
	const handleClick = React.useCallback(() => {
		setSubfactory(factory);
		resetAnchor();
	}, [factory]);

	return (
		<MenuItem onClick={handleClick}>
			<Typography>
				{factory.type}
			</Typography>
		</MenuItem>
	);
}

const styles = theme => ({
	fab: {
		position: 'absolute',
		right: -theme.spacing.unit * 2.5,
	},
});

type Prop = {
	className: string,
	classes: typeof styles,
	enabled: boolean,
	unit: Unit,
	setSubfactory: Function,
};

function AddFactory({className, enabled, unit, setSubfactory, classes}: Prop) {
	const {anchor, handleAnchor, resetAnchor} = useAnchor();
	const factories = getFactory(unit);

	return (
		<>
			<Grow in={enabled}>
				<Fab
					className={className}
					classes={{root: classes.fab}}
					onClick={handleAnchor}
					size="small"
				>
					<Add />
				</Fab>
			</Grow>
			{several(factories) && (
				<Menu anchorEl={anchor} open={Boolean(anchor)} onClose={resetAnchor}>
					{map(
						(factory: Factory) => (
							<FactoryItem
								{...{
									factory: {parent: unit, ...factory},
									setSubfactory,
									resetAnchor,
									key: factory.type,
								}}
							/>
						),
						factories
					)}
				</Menu>
			)}
		</>
	);
}

export default withStyles(styles)(AddFactory);
