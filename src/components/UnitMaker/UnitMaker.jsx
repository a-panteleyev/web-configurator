// @flow

import * as React from 'react';
import {compose} from 'redux';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';

import type {Factory, Unit} from 'containers/Units/types';
import logger from 'utils/logger';

import PropertyItem from './PropertyItem';

const styles = {
	subtitle: {
		margin: '1em',
	},
};

type Props = {
	factory: Factory,
	classes: Object,
};

function UnitMaker({classes, factory: {properties, type}, ...rest}: Props) {
	const path = [];

	return (
		<Grid key="props" container direction="column">
			{properties.map(prop => (
				<PropertyItem
					key={prop.id}
					className={classes.property}
					property={prop}
					path={path}
					{...rest}
				/>
			))}
		</Grid>
	);
}

export default compose(withStyles(styles))(UnitMaker);
