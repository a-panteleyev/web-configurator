// @flow

import * as React from 'react';
import {compose} from 'redux';
import flatten from 'lodash/fp/flatten';
import head from 'lodash/fp/head';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import getOr from 'lodash/fp/getOr';
import includes from 'lodash/fp/includes';
import isEqual from 'lodash/fp/isEqual';
import map from 'lodash/fp/map';
import memoize from 'lodash/fp/memoize';
import size from 'lodash/fp/size';
import values from 'lodash/fp/values';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';

import logger from 'utils/logger';
import type {Constraint, RangeConstraint, Unit} from 'containers/Units/types';
import BoolProperty from './BoolProperty';
import ScalarProperty from './ScalarProperty';
// import withPropEdit from './withPropEdit';

import type {PropertyItemProp} from './types';

const mapOpts = memoize(type =>
	map(({name, [`value_${type}`]: val}) => (
		<option key={val} value={val}>
			{name || val}
		</option>
	))
);
const getEnum: Unit => Array<Constraint> = get(
	(['enum_constraint', 'items']: Array<string>)
);
const getRange: Unit => RangeConstraint = getOr(
	{},
	(['range_constraint']: Array<string>)
);
const onlyItem = flow([size, isEqual(1)]);

const valueType = {
	string: 'text',
	int32: 'number',
};

const styles = theme => ({
	root: {
		margin: theme.spacing.unit * 2,
		display: 'flex',
	},
	item: {
		flexGrow: 1,
	},
	hidden: {
		visibility: 'hidden',
	},
});

// eslint-disable-next-line import/no-mutable-exports
let PropertyItem; // Workaround: for recursive use

type ItemProps = {
	updateVal: (SyntheticInputEvent<HTMLInputElement>) => void,
	snapToggle: (SyntheticInputEvent<*>, boolean) => void,
	snapshotMode: boolean,
	...PropertyItemProp,
};
function getPropControl(type: string) {
	switch (type) {
		case 'bool':
			return BoolProperty;
		case 'string':
		case 'int32':
		default:
			return ScalarProperty;
	}
}

function PropertyItemAccess({classes, property, path, ...rest}: ItemProps) {
	// TODO: that is ugly, think again
	const unitVal = rest.getUnitProp(path, property.id);
	const isUnitVal = Boolean(unitVal);
	const {[`value_${property.type}`]: value} = unitVal || property;

	const enumConstraint = getEnum(property);
	const {min_int: min, max_int: max} = getRange(property);
	const hasEnum = Boolean(enumConstraint);
	const enumOnly = hasEnum && onlyItem(enumConstraint);
	const Item = getPropControl(property.type);
	const mapTypedOpts = mapOpts(property.type);
	const opts = mapTypedOpts(enumConstraint);
	const updateVal = React.useCallback(
		({
			target: {checked, value: newValue},
		}: {
			target: {checked: boolean, value: string | number},
		}) => {
			const {id, type} = property;

			rest.setUnitProp(path, {
				id,
				type,
				[`value_${type}`]: checked || newValue,
				properties: {},
			});
		},
		[path, property.id, rest.setUnitProp]
	);

	React.useEffect(() => {
		if (hasEnum) {
			const {id, type} = property;

			rest.setUnitProp(path, {
				id,
				type,
				...head(enumConstraint),
				properties: {},
			});
		}
	}, [
		/* hasEnum, property */
	]);

	return [
		// TODO: think carefully that there is no better way
		enumOnly ? (
			<input key={property.id} type="hidden" defaultValue={unitVal} />
		) : (
			<Grid className={classes.root} key={property.id}>
				<Item
					className={classes.item}
					disabled={property.readonly}
					value={value}
					onChange={updateVal}
					name={property.name || property.id}
					select={hasEnum}
					type={valueType[property.type]}
					required={hasEnum}
					inputProps={{min, max}}
				>
					{/* List of constraints if available: */}
					{/* <option key="null" value="" /> */}
					{[...opts]}
				</Item>
			</Grid>
		),
		...flow([
			// Append all properties provided by selected constraint
			find(flow([values, includes(value)])),
			get('properties'),
			map(prop => (
				<PropertyItem
					key={prop.id}
					property={prop}
					path={[...path, property.id]}
					{...rest}
				/>
			)),
			flatten,
		])(enumConstraint),
	];
}

// eslint-disable-next-line prefer-const
PropertyItem = compose(withStyles(styles))(PropertyItemAccess);

export default PropertyItem;
