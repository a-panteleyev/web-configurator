// @flow

import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

import type {ItemProps} from './types';

function BoolProperty({
	className,
	value,
	select,
	name,
	required,
	type,
	...rest
}: ItemProps) {
	return (
		<FormGroup className={className} row>
			<FormControlLabel
				control={
					<Switch
						checked={value}
						inputProps={{required}}
						{...rest}
					/>
				}
				label={name}
			/>
		</FormGroup>
	);
}

BoolProperty.defaultProps = {
	value: false,
}

export default BoolProperty;
