// @flow

import * as React from 'react';
import {compose} from 'redux';
import map from 'lodash/fp/map';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';

import type {Unit} from 'containers/Units/types';
import logger from 'utils/logger';

import PropertyItem from 'components/UnitMaker/PropertyItem';

const styles = {
	subtitle: {
		margin: '1em',
	},
};

type Props = {
	unit: Unit,
	classes: Object,
};

function UnitEditor({classes, unit: {properties}, ...rest}: Props) {
	const path = [];

	return (
		<Grid key="props" container direction="column">
			{map(
				prop => (
					<PropertyItem
						key={prop.id}
						className={classes.property}
						property={prop}
						path={path}
						{...rest}
					/>
				),
				properties
			)}
		</Grid>
	);
}

export default compose(withStyles(styles))(UnitEditor);
