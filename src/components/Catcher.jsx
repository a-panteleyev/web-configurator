// @flow

import * as React from 'react';
import cn from 'classnames';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import isEqual from 'lodash/fp/isEqual';
import size from 'lodash/fp/size';
import update from 'lodash/fp/update';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';
import Switch from '@material-ui/core/Switch';
import Add from '@material-ui/icons/Add';
import Check from '@material-ui/icons/Check';
import IconButton from '@material-ui/core/IconButton';
import {withStyles} from '@material-ui/core/styles';
import type {Uid, Unit} from 'containers/Units/types';
import Switcher from 'components/Switcher';

const hasNoChildren = flow([get('units'), size, isEqual(0)]);

const styles = theme => ({
	catcher: {
		display: 'inline-flex',
		borderWidth: 1,
		borderColor: 'transparent',
		borderStyle: 'solid',
		borderRadius: theme.shape.borderRadius,
		margin: theme.spacing.unit / 2,
		transition: [['all', `${theme.transitions.duration.shortest}ms`]],
	},
	button: {
		right: 0,
	},
	hover: {
		borderColor: theme.palette.action.active,
	},
	snapshot: {
		cursor: 'pointer',
	},
	included: {
		color: theme.palette.secondary.main,
		backgroundColor: theme.palette.action.selected,
	},
	placeholder: {
		visibility: 'hidden',
		width: theme.spacing.unit * 4,
		height: theme.spacing.unit * 4,
	},
});

type Props = {
	classes: Object,
	children: React.Element<*>,
	path: Array<Uid>,
	snapshotMode: boolean,
	editMode: boolean,
	toggleTmpl: Function,
	inTmpl: Function,
	changeUnit: Function,
	unit: Unit,
};
function Catcher({
	classes,
	children,
	path,
	snapshotMode,
	editMode,
	toggleTmpl,
	inTmpl,
	changeUnit,
	unit,
}: Props) {
	/** * Template ** */
	const selected = inTmpl(path);
	const onClick = React.useCallback(
		(e: SyntheticMouseEvent) => {
			toggleTmpl(path, !selected);
		},
		[path, selected]
	);
	/** * Hover ** */
	const [hover, setHover] = React.useState(false);
	const handleMouseIn = React.useCallback((e: SyntheticMouseEvent) => {
		e.stopPropagation();
		setHover(true);
	}, []);
	const handleMouseOut = React.useCallback(() => setHover(false), [null]);

	const containerClasses = cn(classes.catcher, {
		[classes.included]: selected,
		[classes.snapshot]: snapshotMode,
		[classes.hover]: snapshotMode && hover,
	});

	/** * Enable/Disable ** */
	const isActive = isEqual('UNIT_STATUS_ACTIVE', unit.status);
	const toggleClass = cn(
		classes.toggle
		// 	{
		// 	[classes.toggleInvisible]: !editMode,
		// 	[classes.toggleVisible]: editMode,
		// }
	);
	const toggleUnit = React.useCallback(() => {
		changeUnit({
			units: [
				update(
					['properties', 'enabled', 'value_int32'],
					(enabled: number): number => Number(!enabled),
					unit
				),
			],
		});
	}, []);

	{
		/* }
		<Grid
			classes={{container: containerClasses}}
			container
			wrap="nowrap"
			alignItems="baseline"
			onMouseOver={handleMouseIn}
			onMouseOut={handleMouseOut}
			onBlur={handleMouseOut}
			container
			direction="row"
			zeroMinWidth
		>
		{ */
	}

	const handlers = (
		<Switcher key="switch" flags={[snapshotMode, editMode]}>
			<Grow in={snapshotMode}>
				<IconButton
					classes={{root: classes.button}}
					onClick={onClick}
					color={selected ? 'primary' : 'default'}
				>
					{selected ? <Check /> : <Add /> /* Transition */}
				</IconButton>
			</Grow>
			{hasNoChildren(unit) && (
				<Grow in={editMode}>
					<Switch
						className={toggleClass}
						checked={isActive}
						onChange={toggleUnit}
					/>
				</Grow>
			)}
		</Switcher>
	);

	return React.cloneElement(
		children,
		{
			hover: {
				onFocus: handleMouseIn,
				onMouseOver: handleMouseIn,
				onMouseOut: handleMouseOut,
				onBlur: handleMouseOut,
			},
			className: containerClasses,
		},
		[...children.props.children, handlers]
	);
}

export default withStyles(styles)(Catcher);
