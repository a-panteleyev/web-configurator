// @flow

import * as React from 'react';
import {compose} from 'redux';
import {injectIntl, intlShape} from 'react-intl';
import InputBase from '@material-ui/core/InputBase';
import {withStyles} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import {withSearch} from 'containers/Search';

const styles = theme => ({
	box: {
		display: 'flex',
		alignItems: 'center',
		borderRadius: theme.shape.borderRadius,
		background: theme.palette.divider,
		border: '2px solid transparent',
	},
	field: {
		paddingLeft: theme.spacing.unit * 2,
	},
	icon: {
		marginLeft: theme.spacing.unit * 2,
	},
});

type Props = {
	classes: Object,
	search: string,
	searchSet: Function,
	intl: intlShape,
};

const ref = React.createRef();
const focus = (e: SyntheticKeyboardEvent<HTMLInputElement>) => {
	if (
		e.key === 's' &&
		e.target.nodeName !== 'INPUT' &&
		ref.current &&
		ref.current !== document.activeElement
	) {
		ref.current.focus();
	}
};
function SearchField({classes, intl, search, searchSet}: Props) {
	const [focused, setFocus] = React.useState(false);
	const updateVal = ({
		target: {value},
	}: SyntheticInputEvent<HTMLInputElement>) => searchSet(value);

	React.useEffect(() => {
		window.addEventListener('keyup', focus);

		return () => window.removeEventListener('keyup', focus);
	});

	return (
		<div
			className={classes.box}
			style={{borderColor: focused ? 'currentColor' : 'transparent'}}
		>
			<SearchIcon className={classes.icon} />
			<InputBase
				classes={{root: classes.field}}
				onFocus={() => setFocus(true)}
				onBlur={() => setFocus(false)}
				onChange={updateVal}
				value={search}
				variant="filled"
				placeholder={intl.formatMessage({id: 'search.placeholder'})}
				inputRef={ref}
			/>
		</div>
	);
}

export default compose(
	injectIntl,
	withSearch,
	withStyles(styles)
)(SearchField);
