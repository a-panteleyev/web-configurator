// @flow

import * as React from 'react';
import initial from 'lodash/fp/initial';
import last from 'lodash/fp/last';
import reduce from 'lodash/fp/reduce';
import * as prop from 'utils/props';
import {cloneStructure} from 'utils/units';
import type {Unit} from 'containers/Unit/type';

const makeStrPath = (path, id) => [...path, id].join('\0');
const revertStrPath = pathStr => pathStr.split('\0');

export default function useTmplCollector() {
	const [template, setTemplate] = React.useState(new Set());
	const toggleTmplProp = (
		path: Array<string>,
		id: string,
		wantSet: boolean
	) => {
		const pathStr = makeStrPath(path, id);

		if (wantSet) {
			template.add(pathStr);
		} else {
			template.delete(pathStr);
		}
		setTemplate(new Set(template));
	};
	const inTmplProp = (path: Array<string>, id: string) =>
		template.has(makeStrPath(path, id));
	const getTmpl = (unit: Unit) => {
		const blankStructure = cloneStructure(unit);

		return reduce(
			(tmpl, pathStr) => {
				const pathArr = revertStrPath(pathStr);
				const path = initial(pathArr);
				const id = last(pathArr);

				return prop.set(tmpl, path, prop.get(unit, path, id));
			},
			blankStructure,
			Array.from(template)
		);
	};
	const resetTmpl = () => setTemplate(new Set());

	return {toggleTmplProp, resetTmpl, inTmplProp, getTmpl};
}
