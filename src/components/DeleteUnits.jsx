// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';

import {withUnitSet} from 'containers/Units';
import type {RmUnitAction, UnitList} from 'containers/Units/types';

type Props = {
	rmUnit: RmUnitAction,
	unitSet: UnitList,
	onDone: Function,
};

function DeleteUnits({rmUnit, unitSet, onDone}: Props) {
	const removeUnits = React.useCallback(() => {
		rmUnit(unitSet); // TODO: resolve Uid to actual Units
		if (onDone) {
			onDone();
		}
	});
	return (
		<Button onClick={removeUnits}>
			<FormattedMessage id="unit.remove" />
			<DeleteIcon />
		</Button>
	);
}

export default compose(withUnitSet)(DeleteUnits);
