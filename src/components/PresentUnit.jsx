// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import get from 'lodash/fp/get';
import map from 'lodash/fp/map';
import sample from 'lodash/fp/sample';
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CardActions from '@material-ui/core/CardActions';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Add from '@material-ui/icons/Add';
import * as colors from '@material-ui/core/colors';

import {flatUnit} from 'utils/units';
import logger from 'utils/logger';

import {withUnit, getUnitByUid, unitType} from 'containers/Units';
// import {withUnitCollector} from 'components/UnitMaker';
import useUnitCollector from 'containers/Units/UnitCollector';
import AddSubUnit from 'components/AddSubUnit';
import * as Cam from 'components/Camera';
import Catcher from 'components/Catcher';
import type {
	Factory,
	Unit,
	UnitType,
	ChangeUnitAction,
	RequestUnitAction,
} from 'containers/Units/types';
import type {SetTmplAction} from 'containers/Templates/types';

const getProperties = get('properties');
const getUnits = get('units');
const getName = ({type, display_id}) => `${type} ${display_id}`;
const styles = theme => ({
	actions: {
		boxShadow: [
			[
				0,
				theme.spacing.unit,
				theme.spacing.unit * 3,
				0,
				theme.palette.common.black,
			],
		],
	},
	units: {
		maxHeight: window.innerHeight - theme.spacing.unit * 20,
		overflow: 'auto',
	},
});

const StyledDivider = withStyles(theme => ({
	root: {
		margin: [[theme.spacing.unit * 5, 0]],
		'&:last-child': {
			display: 'none',
		},
	},
}))(Divider);
StyledDivider.displayName = 'StyledDivider';

const StyledTitle = withStyles(theme => ({
	root: {
		marginTop: theme.spacing.unit * 2,
		marginLeft: theme.spacing.unit * 2,
	},
}))(Typography);
StyledTitle.displayName = 'StyledTitle';

const getComponent = (type: UnitType): React.ComponentType<any> | null => {
	switch (type) {
		case unitType.CAM:
			return Cam.Camera;
		case unitType.CAM_CHAN:
			return Cam.VideoChannel;
		case unitType.CAM_STREAM:
			return Cam.Streaming;
		case unitType.CAM_MIC:
			return Cam.Microphone;
		case unitType.CAM_STOR:
			return Cam.EmbeddedStorage;
		case unitType.CAM_ARCH:
			return Cam.ArchCtx;
		case unitType.CAM_PTZ:
			return Cam.Ptz;
		case unitType.CAM_IO:
			return Cam.Io;
		case unitType.CAM_SPK:
			return Cam.Speaker;
		default:
			return null;
	}
};

type Props = {
	classes: Object,
	unit: Unit,
	addTmpl: SetTmplAction,
	changeUnit: ChangeUnitAction,
	requestUnit: RequestUnitAction,
	snapshotMode: boolean,
	setSubfactory: Factory => void,
	editMode: boolean,
	setEdit: Function,
	toggleTmpl: Function,
	inTmpl: Function,
	onClose: Function,
	addUnit: Function,
	rmUnit: Function,
	changeUnit: Function,
};

function PresentUnit({
	classes,
	onClose,
	unit: superUnit,
	editMode,
	snapshotMode,
	setSubfactory,
	setEdit,
	toggleTmpl,
	inTmpl,
	addUnit,
	rmUnit,
	changeUnit,
}: Props) {
	const mapUnit = path => (unit: Unit) => {
		const Component = getComponent(unit.type);

		if (!Component) {
			return null;
		}

		return (
			<React.Fragment key={unit.uid}>
				<Catcher
					{...{
						path,
						snapshotMode,
						toggleTmpl,
						inTmpl,
						editMode,
						changeUnit,
						unit,
					}}
				>
					<Component
						{...{
							unit,
							editMode,
							addUnit,
							changeUnit,
							rmUnit,
							setEdit,
							setSubfactory,
						}}
					>
						{map(u => mapUnit([...path, u.uid])(u), unit.units)}
					</Component>
				</Catcher>
				{/* editMode && <AddSubUnit factory={unit.factory} {...{setSubfactory}} /> */}
			</React.Fragment>
		);
	};

	return mapUnit([])(superUnit);
}

export default compose(
	withStyles(styles),
	withUnit
)(PresentUnit);
