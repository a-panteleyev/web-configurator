// @flow

import * as React from 'react';
import {compose, bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import get from 'lodash/fp/get';
import map from 'lodash/fp/map';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';

import logger from 'utils/logger';
import {getCustomTmplId} from 'utils/uuid';
import {withUnit, getUnitByUid} from 'containers/Units';
import {withTemplates} from 'containers/Templates';
// import {withUnitCollector} from 'components/UnitMaker';
import Collector from 'containers/Units/Collector';
import PropertyItem from 'components/UnitMaker/PropertyItem';
import TmplCollector from 'containers/TemplateCollector';
import type {Unit as UnitType, RequestUnitAction} from 'containers/Units/types';

const getProperties = get('properties');
const getUnits = get('units');
const getName = ({type, display_id}) => `${type} ${display_id}`;
const styles = theme => ({
	root: {
		display: 'inline-block',
		backgroundColor: theme.palette.background.paper,
		margin: theme.spacing.unit,
		maxWidth: 400,
	},
	extension: {
		display: 'inline-block',
		backgroundColor: theme.palette.background.default,
		color: theme.palette.primary.contrastText,
		margin: theme.spacing.unit,
		maxWidth: 300,
	},
	icon: {
		color: theme.palette.grey[500], // TODO: add to theme
		fontSize: 32,
		// margin: theme.spacing.unit,
	},
});

type Props = {
	classes: Object,
	extend: string => void,
	collapse: string => void,
	extension: boolean,
	unit: UnitType,
	// uid: string,
	requestUnit: RequestUnitAction,
	updateUnit: Function,
	getUnit: Function,
	getTmpl: Function,
};
type State = {};

class Unit extends React.Component<Props, State> {
	componentDidMount() {
		const {
			requestUnit,
			unit,
		} = this.props;

		requestUnit([unit]);
	}

	submit = () => {
		const {changeUnit, getUnit} = this.props;

		changeUnit({
			units: [getUnit()],
		});
	};

	snapshot = () => {
		const {
			unit: {type, uid, display_id},
			getTmpl,
			addTmpl,
		} = this.props;
		const template = {
			id: getCustomTmplId(uid),
			name: `${type}.${display_id}`,
			unit: {
				type,
				units: [],
				opaque_params: [],
				...getTmpl(),
			},
		};

		logger.debug('addTemplate', template);
		addTmpl(template);
	};

	render() {
		const {extend, extension, collapse, classes, ...rest} = this.props;
		const {unit} = rest;
		const children = getUnits(unit);
		const properties = getProperties(unit);
		const path = [];

		// TODO: handle read-only properties in <PropertyItem />

		logger.debug(unit);

		// <Card className={extension ? classes.extension : classes.root}>
		return (
			<TmplCollector>
				<CardHeader
					action={(
						<IconButton onClick={collapse.bind(null, unit.uid)}>
							<CloseIcon />
						</IconButton>
)}
					title={getName(unit)}
					subheader={unit.status}
				/>
				<CardContent>
					{/* JSON.stringify(unit) */}
					{// TODO: Indexify unit
					map(prop => (
						<PropertyItem key={prop.id} property={prop} path={path} {...rest} />
						// <div key={prop.id}>
						// 	<TextField
						// 		disabled={prop.readonly}
						// 		label={prop.name}
						// 		value={String(prop[`value_${prop.type}`])}
						// 	/>
						// </div>
					))(properties)}
				</CardContent>
				{/*
				<CardActions>
					{map(child => (
						<Button key={child.uid} onClick={extend.bind(null, child.uid)}>
							{getName(child)}
						</Button>
					))(children)}
				</CardActions>
				*/}
				<CardActions>
					<Button onClick={this.submit}>
						<FormattedMessage id="unit.submit" />
					</Button>
					<Button onClick={this.snapshot}>
						<FormattedMessage id="unit.snapshot" />
					</Button>
				</CardActions>
			</TmplCollector>
		);
	}
}
// </Card>

export default compose(
	withStyles(styles),
	withUnit,
	withTemplates
)(Unit);
