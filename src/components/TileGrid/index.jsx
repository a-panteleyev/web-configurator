// @flow
import * as React from 'react';
import debounce from 'lodash/fp/debounce';
import inRange from 'lodash/fp/inRange';
import {withStyles} from '@material-ui/core/styles';

const DELAY = 250;

const animId = 'tile-slide-top';
const styles = theme => ({
	[`@keyframes ${animId}`]: {
		from: {
			marginTop: theme.spacing.unit * 4,
			// opacity: 0,
		},
		to: {
			marginTop: 0,
			// opacity: 1,// TODO: Creates unneccessary stacking context
		},
	},
	grid: {
		margin: 'auto',
		position: 'relative',
		top: theme.spacing.unit * 4,
	},
	tile: {
		animationDuration: theme.transitions.duration.shortest,
		animationDelay: theme.transitions.duration.short,
		animationFillMode: 'both',
		animationName: animId,

		position: 'absolute',
		transition: [
			['opacity', `${theme.transitions.duration.shortest}ms`],
			['left', `${theme.transitions.duration.shortest}ms`],
			['top', `${theme.transitions.duration.shortest}ms`],
		],
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		willChange: 'left, top',
	},
});
type Props = {
	classes: Object,
	children: React.Node,
};

// const Tile = withStyles(theme => ({
// 	tile: {
// 		animationDuration: theme.transitions.duration.shortest,
// 		animationDelay: theme.transitions.duration.short,
// 		animationFillMode: 'both',
// 		animationName: animId,
//
// 		position: 'absolute',
// 		transition: [
// 			['opacity', `${theme.transitions.duration.shortest}ms`],
// 			['left', `${theme.transitions.duration.shortest}ms`],
// 			['top', `${theme.transitions.duration.shortest}ms`],
// 		],
// 		transitionTimingFunction: theme.transitions.easing.easeInOut,
// 		willChange: 'left, top',
// 	},
// }))(
// 	({
// 		children,
// 		classes,
// 		idx,
// 		grid,
// 	}: {
// 		children: React.Element,
// 		classes: Object,
// 		idx: number,
// 		grid: React.Ref<HTMLElement>,
// 	}) => {
// 		const [isVisible, setVisible] = React.useState(false);
// 		const ref = React.createRef();
//
// 		React.useEffect(() => {
// 			const checkVisibility = debounce(DELAY, () => {
// 				const tile = ref.current;
//
// 				if (!tile) {
// 					return;
// 				}
//
// 				const visible = inRange(
// 					window.pageYOffset,
// 					window.pageYOffset + window.innerHeight,
// 					tile.offsetTop + tile.offsetHeight / 2
// 				);
//
// 				setVisible(visible);
// 			});
//
// 			grid.current.parentNode.addEventListener('scroll', checkVisibility);
//
// 			return () =>
// 				grid.current.parentNode.removeEventListener('scroll', checkVisibility);
// 		});
// 		return (
// 			<span className={classes.tile} data-idx={idx} ref={ref}>
// 				{React.cloneElement(children, {isVisible})}
// 			</span>
// 		);
// 	}
// );

class TileGrid extends React.Component<Props> {
	grid = React.createRef();

	scheduleReflow = debounce(DELAY, () =>
		requestAnimationFrame(() => this.reflow())
	);

	observer = new MutationObserver(this.scheduleReflow);

	observeConf = {
		attributes: true,
		childList: true,
		subtree: true,
	};

	componentDidMount() {
		if (this.grid.current) {
			this.observer.observe(this.grid.current, this.observeConf);
		}
		window.addEventListener('resize', this.scheduleReflow);
		this.reflow();
	}

	componentDidUpdate() {
		this.reflow();
	}

	componentWillUnmount() {
		this.observer.disconnect();
	}

	reflow() {
		const grid = this.grid.current;
		if (!grid || grid.childElementCount === 0) {
			return;
		}

		let domChild = grid.lastChild;
		const tileWidth = domChild.clientWidth;
		const cols = Math.floor(window.innerWidth / tileWidth);
		const boundary = new Array(cols).fill(0);

		grid.style.width = `${Math.min(cols, grid.childElementCount) *
			tileWidth}px`;
		grid.style.visibility = 'initial';

		while (domChild) {
			if (domChild.dataset.hidden) {
				continue; // eslint-disable-line no-continue
			}

			const idx = Number(domChild.dataset.idx);
			const curCol = idx % cols;

			domChild.style.left = `${tileWidth * curCol}px`;
			domChild.style.top = `${boundary[curCol]}px`;

			boundary[curCol] += domChild.offsetHeight;

			domChild = domChild.previousSibling;
		}
		grid.style.height = `${Math.max(...boundary)}px`;
	}

	render() {
		const {classes, children} = this.props;

		return (
			<div
				ref={this.grid}
				className={classes.grid}
				onTransitionEnd={this.scheduleReflow}
				onAnimationEnd={this.scheduleReflow}
			>
				{Array.isArray(children) &&
					children
						.map((child, idx) => (
							<span key={child.key} className={classes.tile} data-idx={idx}>
								{child}
							</span>
						))
						.reverse()}
			</div>
		);
	}
}

export default withStyles(styles)(TileGrid);
