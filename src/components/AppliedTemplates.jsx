// @flow

import * as React from 'react';
import Chip from '@material-ui/core/Chip';

import {withTemplates} from 'containers/Templates';

type Props {
	template_ids: Array<string>,
};

const AppliedTemplates = ({template_ids}: Props) => <>
		template_ids.map(id => <Chip label={id} />)
	</>;

export default withTemplates(AppliedTemplates);
