// @flow

import * as React from 'react';

type Props = {
	children: React.Node,
	flags: Array<boolean>,
};

export default function Switcher({children, flags}: Props) {
	const idx = flags.indexOf(true);
	const child = React.Children.toArray(children)[idx] || <div />;

	return child;
}
