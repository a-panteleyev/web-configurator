// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import cn from 'classnames';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import gt from 'lodash/fp/gt';
import isEqual from 'lodash/fp/isEqual';
import size from 'lodash/fp/size';
import some from 'lodash/fp/some';
import placeholder from 'lodash/fp/placeholder';
import tap from 'lodash/fp/tap';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles, withTheme} from '@material-ui/core/styles';
import CSSTransition from 'react-transition-group/CSSTransition';

// import AVDetector from 'components/AVDetector';
// import ArchiveContext from 'components/ArchiveContext';
// import {unitTypes} from 'container/Units';
import {withUnits} from 'containers/Units';
import useUnitCollector from 'containers/Units/UnitCollector';
import UnitEditor from 'components/UnitEditor';
import type {Unit} from 'container/Units/types';
import {flatUnit} from 'utils/units';
import logger from 'utils/logger';

const hasOptions = flow([
	get('properties'),
	some(flow([get(['enum_constraint', 'items']), size, gt(placeholder, 1)])),
]);
const isStripped = flow([get('stripped'), isEqual(true)]);

const animId = 'slide';
const styles = theme => ({
	root: {
		backgroundColor: theme.palette.background.paper,
		animation: [[`${theme.transitions.duration.standard}ms`, animId]],
		animationFillMode: 'forwards',
		animationTimingFunction: theme.transitions.easing.easeOut,
		position: 'absolute',
		top: 0,
		right: 0,
		zIndex: 1,
		height: '100%',
		overflowX: 'hidden',
		overflowY: 'auto',
		padding: -theme.spacing.unit * 4,
	},
	enter: {
		maxWidth: 0,
		minWidth: 0,
	},
	enterActive: {
		maxWidth: '100%',
		minWidth: '100%',
		transition: [
			[
				'all',
				`${theme.transitions.duration.standard}ms`,
				theme.transitions.easing.easeOut,
			],
		],
	},
	exit: {
		composes: 'enterActive',
	},
	exitActive: {
		composes: 'enter',
	},
	[`@keyframes ${animId}`]: {
		from: {
			maxWidth: 0,
			minWidth: 0,
		},
		to: {
			maxWidth: '100%',
			minWidth: '100%',
		},
	},
	empty: {
		composes: '$root',
		animationDirection: 'reverse',
	},
});

// function getEditor(type: string): React.ComponentType<*> | string {
// 	switch (type) {
// 		case unitTypes.CAM_AVDETECT:
// 			return AVDetector;
// 		case unitTypes.CAM_ARCH:
// 			return ArchiveContext;
// 		default:
// 			return 'div';
// 	}
// }

type Props = {
	addUnit: ApplyUnitAction,
	classes: Object,
	changeUnit: Function,
	requestUnit: Function,
	unit: Unit,
	root: Unit,
	onCancel: Function,
	theme: Object,
};

function SubUnitEditor({
	classes,
	unit,
	root,
	changeUnit,
	requestUnit,
	onCancel,
	theme,
}: Props): React.Element<*> | null {
	const [show, setShow] = React.useState(false);
	const snapshot = useUnitCollector(unit);
	const submitUnit = React.useCallback(() => {
		const units = flatUnit(snapshot.getSnapshot());

		logger.log('submit unit', units);
		// changeUnit({units});
		setShow(false);
	}, [snapshot.getSnapshot]);
	const handleCancel = React.useCallback(() => setShow(false), []);

	React.useEffect(() => {
		if (unit) {
			logger.log('hello');
			setShow(true);
		}
	}, [unit]);

	return (
		<CSSTransition
			in={show}
			classNames={classes}
			mountOnEnter
			unmountOnExit
			timeout={theme.transitions.duration.short}
			onExited={onCancel}
		>
			<Grid container className={classes.root}>
				<UnitEditor onClose={handleCancel} {...{unit, ...snapshot}} />
				<Button onClick={submitUnit}>
					<FormattedMessage id="unit.apply" />
				</Button>
				<Button onClick={handleCancel}>
					<FormattedMessage id="unit.cancel" />
				</Button>
			</Grid>
		</CSSTransition>
	);
}

export default compose(
	withStyles(styles),
	withTheme()
)(SubUnitEditor);
