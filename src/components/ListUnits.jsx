// @flow

import * as React from 'react';
import {compose} from 'redux';
import cx from 'classnames';
import memoize from 'lodash/fp/memoize';
import {withStyles} from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import logger from 'utils/logger';

import {withType} from 'containers/Units';
import TileGrid from 'components/TileGrid';
import UnitItem from 'components/UnitItem';

import type {
	RequestUnitAction,
	Unit,
	UnitList,
	UnitType,
} from 'containers/Units/types';
import {withSelection, SelectionType} from 'containers/Selection';
import {withWorkbench} from 'containers/Workbench';

const listStyles = theme => ({
	box: {
		margin: theme.spacing.unit,
		'&:hover > svg': {
			opacity: 1,
		},
	},
	item: {
		border: '2px solid transparent',
		transition: theme.transitions.easing.easingInOut,
		width: theme.spacing.unit * 35,
		position: 'relative',
	},
	selectBtn: {
		backgroundColor: theme.palette.background.paper,
		borderRadius: '50%',
		position: 'absolute',
		opacity: 0,
		top: -theme.spacing.unit * 0.5,
		left: -theme.spacing.unit * 0.5,
		transition: `opacity ${theme.transitions.duration.standard}ms`,
		zIndex: 2,
	},
	selected: {
		borderColor: 'currentColor',
	},
	notselected: {
		borderColor: 'transparent',
	},
});
type ListProps = {
	classes: Object,
	units: UnitList,
	requestUnit: RequestUnitAction,
	selection: SelectionType,
	select: Function,
	setUnit: Function,
	type: UnitType,
	deselect: Function,
};

const getHandler = memoize((unit: Unit, setUnit) => (e: MouseEvent) => {
	e.stopPropagation();
	logger.debug('add unit to Workbench', unit);
	setUnit(unit.uid);
});

const List = compose(
	withStyles(listStyles),
	withSelection,
	withWorkbench
)(
	({
		units,
		type,
		classes,
		selection,
		select,
		deselect,
		requestUnit,
	}: ListProps) => {
		const handleSelect = ({
			currentTarget: {dataset},
		}: SyntheticMouseEvent<HTMLElement>) => {
			const {uid} = dataset;
			const action = selection.has(uid) ? deselect : select;

			action(uid);
		};

		return (
			<TileGrid>
				{units.map(unit => {
					const {uid} = unit;
					const selected = selection.has(unit.uid);
					const className = cx(classes.item, {
						[classes.selected]: selected,
					});

					return (
						<div className={classes.box} key={uid}>
							<CheckCircleIcon
								className={classes.selectBtn}
								data-uid={uid}
								data-selected={selected}
								onClick={handleSelect}
							/>
							<UnitItem {...{className, type, requestUnit, uid}} />
						</div>
					);
				})}
			</TileGrid>
		);
	}
);

const containerStyles = {
	listContainer: {
		flex: 'auto',
		overflowY: 'auto',
		overflowX: 'hidden',
	},
};

type Props = {
	classes: Object,
	type: UnitType,
};

const ListUnits = ({classes, type}: Props) => {
	const ListOfType = withType(type)(List);

	return (
		<div className={classes.listContainer}>
			<ListOfType />
		</div>
	);
};

export default withStyles(containerStyles)(ListUnits);
