// @flow

import * as React from 'react';
import {compose} from 'redux';
import map from 'lodash/fp/map';
import noop from 'lodash/fp/noop';
import size from 'lodash/fp/size';
import {FormattedMessage} from 'react-intl';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import DeleteForever from '@material-ui/icons/DeleteForever';
import {withStyles} from '@material-ui/core/styles';

import {withTemplates} from 'containers/Templates';
import * as TmplTypes from 'containers/Templates/types';
import type {Uid} from 'containers/Units/types';

const styles = theme => ({
	assign: {
		margin: theme.spacing.unit,
	},
});

function TemplateItem({
	template: {
		body: {id, name},
	},
	templateIds,
	setTemplateIds,
}: {
	template: TmplTypes.Template,
	templateIds: Array<TmplTypes.Tid>,
	setTemplateIds: Function,
}) {
	const selected = templateIds.has(id);
	const toggle = React.useCallback(() => {
		if (selected) {
			templateIds.delete(id);
		} else {
			templateIds.add(id);
		}
		setTemplateIds(new Set(templateIds));
	}, [id, templateIds]);

	return (
		<MenuItem button {...{selected}} onClick={toggle}>
			{name}
		</MenuItem>
	);
}

type Props = {
	classes: Object,
	assignTmpl: TmplTypes.AssignTmplAction,
	getTmpl: TmplTypes.GetTmplAction,
	rmTmpl: TmplTypes.RmTmplAction,
	templates: TmplTypes.TemplateIndex,
	uids: Array<Uid>,
	onDone: Function,
};

function TemplateAssign({
	assignTmpl,
	rmTmpl,
	classes,
	getTmpl,
	templates,
	uids,
	onDone,
}: Props) {
	const [anchorElm, setAnchorElm] = React.useState(null);
	const [templateIds, setTemplateIds] = React.useState(new Set());
	const openMenu = React.useCallback(({currentTarget}) => {
		getTmpl();
		setAnchorElm(currentTarget);
	}, []);
	const closeMenu = React.useCallback(() => setAnchorElm(null), []);
	const assign = React.useCallback(() => {
		assignTmpl({
			templateIds: [...templateIds],
			uids,
		});
		onDone();
		closeMenu();
	}, [templateIds, uids]);
	const remove = React.useCallback(() => {
		rmTmpl([...templateIds]);

		onDone();
	}, [templateIds]);

	return (
		<div>
			<Button variant="text" onClick={openMenu}>
				<FormattedMessage id="template.assign" />
				<PhotoCamera />
			</Button>
			<Menu anchorEl={anchorElm} open={Boolean(anchorElm)} onClose={closeMenu}>
				{templates.size ? (
					<CircularProgress indeterminate />
				) : (
					map(
						template => (
							<TemplateItem
								key={template.body.id}
								{...{template, templateIds, setTemplateIds}}
							/>
						),
						templates
					)
				)}
				<Button
					className={classes.assign}
					color="primary"
					variant="text"
					onClick={assign}
				>
					<FormattedMessage id="template.submit" />
				</Button>
				<IconButton onClick={remove}>
					<DeleteForever />
				</IconButton>
			</Menu>
		</div>
	);
}

export default compose(
	withStyles(styles),
	withTemplates
)(TemplateAssign);
