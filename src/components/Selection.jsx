// @flow

import * as React from 'react';
import {compose} from 'redux';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    transition: theme.transitions.easing.easingInOut,
  },
  selected: {
    opacity: 0.5,
  }
});

type Props = {
  children: React.Node,
  selected: bool,
  select: Function,
  deselect: Function,
};

const Selection = (({classes, children, selected, ...rest}): Props) =>
  (
    <div
      className={selected ? classes.selected : classes.root}
      {...rest}
    >
      {children}
    </div>
  );

export default compose(
  withStyles(styles),
)(Selection);
