import * as React from 'react';
import {compose} from 'redux';
import {Set} from 'immutable';
import {withStyles} from '@material-ui/core/styles';

import {withWorkbench} from 'containers/Workbench';
import Unit from 'components/Unit';
import logger from 'utils/logger';

import type {Unit as UnitType} from 'containers/Units/types';
import type {Set as SetType} from 'immutable';

type Props = {
	classes: Object,
	wbUid: string,
	setUnit: Function,
};

type State = {
	extensions: SetType<string>,
};

const styles = theme => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-even',
		background: theme.palette.action.active,
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
	},
});

class Workbench extends React.Component<Props, State> {
	state = {
		extensions: Set(),
	};

	extend = (uid: string) => {
		this.setState(({extensions}) => ({extensions: extensions.add(uid)}));
		logger.log('extension add', uid);
	};

	collapse = (uid: string) => {
		this.setState(({extensions}) => ({extensions: extensions.delete(uid)}));
		logger.log('extension remove', uid);
	};

	close = () => {
		const {setUnit} = this.props;

		setUnit(null);
		logger.log('Workbench closing');
	};

	clickAway = ({currentTarget, target}: SyntheticMouseEvent) => {
		if (currentTarget === target) {
			this.close();
		}
	};

	render() {
		const {
			props: {wbUid: uid, classes},
			state: {extensions},
		} = this;

		if (!uid) {
			return null;
		}

		return (
			<div className={classes.root} onClick={this.clickAway}>
				<Unit uid={uid} extend={this.extend} collapse={this.close} />
				{extensions.toSeq().map(extunit => (
					<Unit
						key={extunit}
						uid={extunit}
						extension
						extend={this.extend}
						collapse={this.collapse}
					/>
				))}
			</div>
		);
	}
}
export default compose(
	withStyles(styles),
	withWorkbench
)(Workbench);
