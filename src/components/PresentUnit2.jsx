// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import get from 'lodash/fp/get';
import map from 'lodash/fp/map';
import sample from 'lodash/fp/sample';
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CardActions from '@material-ui/core/CardActions';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Add from '@material-ui/icons/Add';
import * as colors from '@material-ui/core/colors';

import {flatUnit} from 'utils/units';
import logger from 'utils/logger';

import {withUnit, getUnitByUid, unitType} from 'containers/Units';
// import {withUnitCollector} from 'components/UnitMaker';
import useUnitCollector from 'containers/Units/UnitCollector';
import AddSubUnit from 'components/AddSubUnit';
import * as Cam from 'components/Camera';
import Catcher from 'components/Catcher';
import UnitShifter from 'components/UnitShifter';
import type {
	Factory,
	Unit,
	ChangeUnitAction,
	RequestUnitAction,
} from 'containers/Units/types';
import type {SetTmplAction} from 'containers/Templates/types';

type Props = {
	classes: Object,
	unit: Unit,
	addTmpl: SetTmplAction,
	changeUnit: ChangeUnitAction,
	requestUnit: RequestUnitAction,
	snapshotMode: boolean,
	setSubfactory: Factory => void,
	editMode: boolean,
	toggleTmpl: Function,
	inTmpl: Function,
	onClose: Function,
	addUnit: Function,
	rmUnit: Function,
};

function PresentUnit(props: Props) {
	const {changeUnit, onClose, unit} = props;
	const snapshot = useUnitCollector(unit);
	const submitUnit = React.useCallback(() => {
		const units = flatUnit(snapshot.getSnapshot());

		logger.log('submit unit', units);
		changeUnit({units});
		onClose();
	}, [snapshot.getSnapshot]);
	return <UnitShifter {...{submitUnit, ...snapshot, ...props}} />;
}

export default compose(withUnit)(PresentUnit);
