// @flow

import * as React from 'react';
import {compose} from 'redux';
import {withType, unitType} from 'containers/Units';
import UnitFactory from 'components/UnitFactory'; // TODO: decide whether to generalise it

import type {UnitType, UnitList} from 'container/Units';

const MakeCam = withType(unitType.NODE)(({units}) => (
	<UnitFactory parents={units} type={unitType.CAM} />
));
const MakeStorage = withType(unitType.NODE)(({units) => (
	<UnitFactory parents={units} type={unitType.STORAGE} />
));
const MakeNode = withType(unitType.DOMAIN)(({units, ...rest}) => (
	<UnitFactory parents={units} type={unitType.NODE} />
));

/*
const styles = theme => ({
	hexPos: {
		paddingTop: theme.spacing.unit * 5,
	},
	hexContent: {
		background: purple[200],
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		display: 'flex',
		flexDirection: 'column',
	},
});
*/

function getMaker(type: UnitType) {
	switch (type) {
		case unitType.NODE:
			return MakeNode;
		case unitType.CAM:
			return MakeCam;
		case unitType.STORAGE:
			return MakeStorage;
		default:
			return 'div';
	}
}

type Props = {
	type: UnitType,
};

function Maker({type}: Props) {
	const MakeUnit = getMaker(type);

	return <MakeUnit />;
}

export default Maker;
