// @flow

import * as React from 'react';
import {unitType} from 'containers/Units';
import * as Cam from 'components/Camera';
import type {Unit, UnitType} from 'containers/Units/type';

const getComponent = (type: UnitType): React.ComponentType<any> | string => {
	switch (type) {
		case unitType.CAM:
			return Cam.Camera;
		case unitType.CAM_CHAN:
			return Cam.VideoChannel;
		case unitType.CAM_STREAM:
			return Cam.Streaming;
		case unitType.CAM_MIC:
			return Cam.Microphone;
		case unitType.CAM_STOR:
			return Cam.EmbeddedStorage;
		case unitType.CAM_PTZ:
			return Cam.Ptz;
		case unitType.CAM_IO:
			return Cam.Io;
		case unitType.CAM_SPK:
			return Cam.Speaker;
		default:
			return 'div';
	}
};

export default function UnitShifter(props: {unit: Unit}) {
	const {unit} = props;
	const Component = getComponent(unit.type);

	return <Component {...props} />;
}
