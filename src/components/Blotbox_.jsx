import * as React from 'react';
import noop from 'lodash/fp/noop';
import IconButton from '@material-ui/core/IconButton';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import {darkTheme} from 'theme';
import ContentAdd from '@material-ui/icons/Add';
import Close from '@material-ui/icons/Close';

const AUTO = 'auto';
const VISIBLE = 'visible';
const HIDDEN = 'hidden';
const NONE = 'none';
const ZERO = '0ms';
const DURATION = '450ms';
const DELAY = '450ms';

/** TODO:
const styles = theme => {
	const size = theme.spacing.unit * 7;
	const miniSize = theme.spacing.unit * 5;

	return {
	root: {
		minHeight: size,
		minWidth: size,
		maxHeight: size,
		maxWidth: size,
		'&--mini': {
			maxHeight: miniSize,
			maxWidth: miniSize,
		},
		'&--open': {
			boxShadow: theme.shadows[13],
			maxHeight: window.innerHeight,
			maxWidth: window.innerWidth,
			'& $button': {
				opacity: 0,
				transitionDelay: 0,
				transitionProperty: 'opacity',
				zIndex: -1,
			},
			'& $icon': {
				opacity: 1,
			}
		}
	},
	button: {
		opacity: 1,
		transitionDelay: theme.transitions.durations.standard,
	},
		icon: {
			opacity: 0,
		},
		ink: {
			backgroundColor: theme.palette.secondary
		},
};
*/

type Props = {
	id: string,
	color: string,
	children: React.Node,
	className: string,
	active: Function,
	icon: React.Element,
	mini: boolean,
	style: Object,
	onClose: Function,
	stopPropagation: Function,
	closeOnClickOutside: boolean,
};

type State = {
	open: boolean,
	isShown: boolean,
};

class Blotbox extends React.Component<Props, State> {
	theme = darkTheme;

	static defaultProps = {
		active: noop,
		onClose: noop,
		closeOnClickOutside: false,
	};

	state = {
		open: false,
		isShown: false,
	};

	// componentDidMount() {
	// 	this.longest.addEventListener(
	// 		'transitionend',
	// 		this.handleChildrenTransition
	// 	);
	// }

	// componentWillUnmount() {
	// 	this.longest.removeEventListener(
	// 		'transitionend',
	// 		this.handleChildrenTransition
	// 	);
	// }

	handleChildrenTransition = e => {
		if (e.target !== e.currentTarget || e.propertyName !== 'opacity') {
			return;
		}
		const {active} = this.props;

		this.setState(
			state => ({isShown: state.open}),
			state => active(state.open)
		);
	};

	open = e => {
		const {active, stopEventPropagation} = this.props;
		if (stopEventPropagation) {
			e.stopPropagation();
		}
		this.setState({open: true, isShown: true}, () => active(true));
	};

	close = () => {
		const {onClose} = this.props;

		this.setState({open: false});
		onClose();
	};

	render() {
		const {
			id,
			children,
			color,
			className,
			icon,
			mini,
			style,
			// adaptive,
			// mobileMode,
			closeOnClickOutside,
		} = this.props;
		const {
			palette: {secondary},
			shadows,
		} = this.theme;

		const {open, isShown} = this.state;

		const openReady = open && isShown;
		const bColor = color || secondary.main;
		const inkColor = bColor; // open ? darken(bColor, 0.05) : bColor;
		const size = mini ? 40 : 56;
		const windowWidth = window.innerWidth;
		const windowHeight = window.innerHeight;
		const containerStyle = {
			...style,
			boxShadow: open ? shadows[13] : NONE,
			minHeight: size + 32,
			minWidth: size + 32,
			maxHeight: open ? windowWidth : size,
			maxWidth: open ? windowHeight : size,
			transitionDelay: !open && ZERO,
			overflow: isShown ? HIDDEN : VISIBLE,
		};
		const buttonStyle = {
			opacity: !open && 1,
			transitionDelay: open ? ZERO : DELAY,
			transitionProperty: 'opacity',
			zIndex: open && -1,
		};
		const iconStyle = {
			opacity: !open && 1,
			transitionDelay: open && ZERO,
		};
		const inkStyle = {
			background: inkColor,
			height: size,
			width: size,
			transform: !open && NONE,
			transitionTimingFunction: open
				? 'cubic-bezier(0.5, 0, 1, 1)'
				: 'cubic-bezier(0, 0.5, 1, 1)',
		};
		const contentStyle = {
			opacity: openReady && 1,
			transitionDelay: !open && ZERO,
			pointerEvents: !open && NONE,
		};
		const closeButtonStyle = {
			pointerEvents: openReady && AUTO,
			transform: openReady ? NONE : 'translateX(100%) rotate(90deg)',
			transitionProperty: 'all',
			transitionDuration: DURATION,
			transitionDelay: openReady ? DELAY : ZERO,
			opacity: open ? 1 : 0,
		};

		return (
			<MuiThemeProvider theme={this.theme}>
				<div className={className} styleName="container" style={containerStyle}>
					<div styleName="ink" style={inkStyle} />
					<Backdrop open={closeOnClickOutside && open} onClick={this.close} />
					<div styleName="content" style={contentStyle}>
						{isShown
							? React.cloneElement(children, {
									onCancel: this.close,
							  })
							: null}
					</div>
					<IconButton
						styleName="close-button"
						style={closeButtonStyle}
						onClick={this.close}
					>
						<Close />
					</IconButton>
					<Button
						variant="fab"
						id={id}
						color="secondary"
						mini={mini}
						styleName="action-button"
						style={buttonStyle}
						onClick={this.open}
						onTransitionEnd={this.handleChildrenTransition}
					>
						{React.cloneElement(icon || <ContentAdd />, {
							style: iconStyle,
						})}
					</Button>
				</div>
			</MuiThemeProvider>
		);
	}
}

export default Blotbox;
