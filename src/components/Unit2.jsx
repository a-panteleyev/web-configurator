// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import get from 'lodash/fp/get';
import map from 'lodash/fp/map';
import sample from 'lodash/fp/sample';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import * as colors from '@material-ui/core/colors';

import {flatUnit} from 'utils/units';
import * as props from 'utils/props';
import {getCustomTmplId} from 'utils/uuid';
import logger from 'utils/logger';

import {withUnit, getUnitByUid} from 'containers/Units';
import {withTemplates} from 'containers/Templates';
// import {withUnitCollector} from 'components/UnitMaker';
import useUnitCollector from 'containers/Units/UnitCollector';
import useTmplCollector from 'containers/TemplateCollector';
import PropertyItem from 'components/UnitMaker/PropertyItem';
import type {
	UnitType,
	ChangeUnitAction,
	RequestUnitAction,
} from 'containers/Units/types';
import type {SetTmplAction} from 'containers/Templates/types';

const getProperties = get('properties');
const getUnits = get('units');
const getName = ({type, display_id}) => `${type} ${display_id}`;
const styles = theme => ({
	actions: {
		boxShadow: [
			[
				0,
				theme.spacing.unit,
				theme.spacing.unit * 3,
				0,
				theme.palette.common.black,
			],
		],
	},
	units: {
		minWidth: theme.spacing.unit * 50,
		maxHeight: window.innerHeight - theme.spacing.unit * 20,
		overflow: 'auto',
	},
});

const StyledDivider = withStyles(theme => ({
	root: {
		margin: [[theme.spacing.unit * 5, 0]],
		'&:last-child': {
			display: 'none',
		},
	},
}))(Divider);

const StyledTitle = withStyles(theme => ({
	root: {
		marginTop: theme.spacing.unit * 2,
		marginLeft: theme.spacing.unit * 2,
	},
}))(Typography);

type Props = {
	classes: Object,
	unit: UnitType,
	addTmpl: SetTmplAction,
	changeUnit: ChangeUnitAction,
	requestUnit: RequestUnitAction,
	onClose: Function,
};

function Unit({
	classes,
	addTmpl,
	changeUnit,
	onClose,
	requestUnit,
	unit,
}: Props) {
	const snapshot = useUnitCollector(unit);
	const [snapshotMode, setSnapshotMode] = React.useState(false);
	const submitUnit = React.useCallback(() => {
		const units = flatUnit(snapshot.getSnapshot());

		logger.log('submit unit', units);
		changeUnit({units});
		onClose();
	}, [snapshot.getSnapshot]);
	const toggleSnapshotMode = React.useCallback(
		() => setSnapshotMode(!snapshotMode),
		[snapshotMode]
	);
	const template = useTmplCollector();
	const submitSnapshot = React.useCallback(() => {
		const tmplUnit = props.toList(template.getTmpl(snapshot.getInternal()));
		const randomColor = sample(colors)['500'];
		const tmpl = {
			id: getCustomTmplId(tmplUnit.uid),
			name: `${tmplUnit.type}.${tmplUnit.display_id}`,
			unit: {
				...tmplUnit,
				opaque_params: [],
			},
		};

		logger.log('submit template', tmpl);
		addTmpl(tmpl);
	}, [template.getTmpl]);
	const mapProp = path => property => (
		<PropertyItem
			key={property.id}
			{...{property, path, snapshotMode, ...snapshot, ...template}}
		/>
	);
	const mapUnit = path => ({
		display_id,
		uid,
		properties,
		type,
		units,
	}: UnitType) => (
		<React.Fragment key={uid}>
			<StyledTitle variant="subtitle1" gutterBottom>
				{type} 
				{' '}
				{display_id}
			</StyledTitle>
			{map(mapProp([...path]), properties)}
			<StyledDivider classes={classes.divider} />
			{map(u => mapUnit([...path, u.uid])(u), units)}
		</React.Fragment>
	);

	React.useEffect(() => {
		if (unit && unit.stripped) {
			requestUnit([unit]);
		}
	});

	return (
		<>
			<div className={classes.units}>
				{mapUnit([])(unit)}
			</div>
			<CardActions className={classes.actions}>
				<Button
					onClick={submitUnit}
					color={snapshotMode ? 'default' : 'primary'}
					variant={snapshotMode ? 'text' : 'contained'}
				>
					<FormattedMessage id="unit.submit" />
				</Button>
				<Button
					onClick={toggleSnapshotMode}
					color={snapshotMode ? 'default' : 'secondary'}
				>
					<FormattedMessage id="unit.snapshot.make" />
				</Button>
				{snapshotMode && (
					<Button variant="contained" color="primary" onClick={submitSnapshot}>
						<FormattedMessage id="unit.snapshot.submit" />
					</Button>
				)}
			</CardActions>
		</>
	);
}

export default compose(
	withStyles(styles),
	withUnit,
	withTemplates
)(Unit);
