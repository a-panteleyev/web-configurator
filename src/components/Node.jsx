// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import NodeIcon from '@material-ui/icons/DeveloperBoard';
import {withStyles} from '@material-ui/core/styles';

import {withWorkbench} from 'containers/Workbench';

import type {Unit} from 'containers/Units/types';

const styles = theme => ({
	root: {
		display: 'flex',
		backgroundColor: theme.palette.grey[200],
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%',
		height: '100%',
	},
});

type Props = {
	unit: Unit,
	classes: Object,
	setUnit: Function,
};

const Node = ({unit: {display_id, status}, classes}: Props) => (
	<>
		<CardHeader
			avatar={<NodeIcon />}
			title={display_id}
			subheader={status && <FormattedMessage id={`unit.status.${status}`} />}
		/>
		<CardContent />
	</>
);

export default compose(withStyles(styles))(Node);
