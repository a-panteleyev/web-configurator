// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import cn from 'classnames';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import gt from 'lodash/fp/gt';
import size from 'lodash/fp/size';
import some from 'lodash/fp/some';
import placeholder from 'lodash/fp/placeholder';
import tap from 'lodash/fp/tap';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {withStyles, withTheme} from '@material-ui/core/styles';
import CSSTransition from 'react-transition-group/CSSTransition';

// import AVDetector from 'components/AVDetector';
// import ArchiveContext from 'components/ArchiveContext';
// import {unitTypes} from 'container/Units';
import {withUnits} from 'containers/Units';
import useUnitCollector from 'containers/Units/UnitCollector';
import UnitMaker from 'components/UnitMaker';
import type {AddUnitAction, Factory} from 'container/Units/types';
import logger from 'utils/logger';

const hasOptions = flow([
	get('properties'),
	some(flow([get(['enum_constraint', 'items']), size, gt(placeholder, 1)])),
]);

const animId = 'slide';
const styles = theme => ({
	root: {
		backgroundColor: theme.palette.background.paper,
		animation: [[`${theme.transitions.duration.standard}ms`, animId]],
		animationFillMode: 'forwards',
		animationTimingFunction: theme.transitions.easing.easeOut,
		position: 'absolute',
		top: 0,
		right: 0,
		zIndex: 1,
		minHeight: '100%',
		overflow: 'auto',
		padding: -theme.spacing.unit * 4,
	},
	enter: {
		maxWidth: 0,
		minWidth: 0,
	},
	enterActive: {
		maxWidth: '100%',
		minWidth: '100%',
		transition: [
			[
				'all',
				`${theme.transitions.duration.standard}ms`,
				theme.transitions.easing.easeOut,
			],
		],
	},
	exit: {
		composes: 'enterActive',
	},
	exitActive: {
		composes: 'enter',
	},
	[`@keyframes ${animId}`]: {
		from: {
			maxWidth: 0,
			minWidth: 0,
		},
		to: {
			maxWidth: '100%',
			minWidth: '100%',
		},
	},
	empty: {
		composes: '$root',
		animationDirection: 'reverse',
	},
});

// function getMaker(type: string): React.ComponentType<*> | string {
// 	switch (type) {
// 		case unitTypes.CAM_AVDETECT:
// 			return AVDetector;
// 		case unitTypes.CAM_ARCH:
// 			return ArchiveContext;
// 		default:
// 			return 'div';
// 	}
// }

function SubUnitMaker({
	addUnit,
	classes,
	factory,
	onCancel,
	theme,
}: {
	addUnit: AddUnitAction,
	classes: Object,
	factory: Factory,
	onCancel: Function,
	theme: Object,
}): React.Element<*> | null {
	const snapshot = useUnitCollector({display_id: Date.now()});
	const [show, setShow] = React.useState(false);
	const handleCancel = React.useCallback(() => setShow(false), []);
	const handleAdd = React.useCallback(() => {
		const {parent, type} = factory;
		const {properties} = snapshot.getSnapshot();

		logger.log('adding %s to %s', type, parent.type);
		addUnit({
			units: [
				{
					...parent,
					units: [{type, properties}],
				},
			],
		});
		setShow(false);
	}, [factory, snapshot.getInternal()]);

	React.useEffect(() => {
		// TODO: think again. also need to choose the-only-option
		if (factory && !hasOptions(factory) && false) {
			const {parent, type, properties} = factory;

			logger.log('factory has no options. creating', factory.type);
			addUnit({
				uid: parent,
				units: [
					{
						type,
						properties,
					},
				],
			});

			return;
		}
		if (factory) {
			setShow(true);
		}
	}, [factory]);

	return (
		<CSSTransition
			in={show}
			classNames={classes}
			mountOnEnter
			unmountOnExit
			timeout={theme.transitions.duration.short}
			onExited={onCancel}
		>
			<div className={classes.root}>
				<UnitMaker {...{factory, ...snapshot}} />
				<Button onClick={handleAdd}>
					<FormattedMessage id="unit.add" />
				</Button>
				<Button onClick={handleCancel}>
					<FormattedMessage id="unit.cancel" />
				</Button>
			</div>
		</CSSTransition>
	);
}

export default compose(
	withStyles(styles),
	withUnits,
	withTheme()
)(SubUnitMaker);
