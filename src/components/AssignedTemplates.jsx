// @flow

import * as React from 'react';
import {compose} from 'redux';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import isEqual from 'lodash/fp/isEqual';
import map from 'lodash/fp/map';
import noop from 'lodash/fp/noop';
import tap from 'lodash/fp/tap';
import update from 'lodash/fp/update';
import without from 'lodash/fp/without';
import ListItem from '@material-ui/core/ListItem';
import Chip from '@material-ui/core/Chip';
import {withStyles, withTheme} from '@material-ui/core/styles';

import {withTemplates} from 'containers/Templates';
import type {
	Template,
	TemplateIndex,
	AssignTmplAction,
} from 'containers/Templates/types';
import type {Unit} from 'containers/Unit/types';

type Props = {
	unit: Unit,
	templates: TemplateIndex,
	assignTmpl: AssignTmplAction,
};

const getColor = flow([
	find(flow([get('id'), isEqual('color')])),
	get('value_string'),
]);

const ThemedChip = withTheme()(
	({
		theme,
		color = theme.palette.action.disabled,
		...props
	}: {
		color: string,
		theme: Object,
	}) => (
		<Chip
			style={{
				color: theme.palette.getContrastText(color),
				backgroundColor: color,
			}}
			{...props}
		/>
	)
);

function AssignedTemplate({
	id,
	templates,
	assigned,
	assignTmpl,
	editMode,
	unit,
}: {
	id: Tid,
	templates: TemplateIndex,
	assigned: Array<Tid>,
	assignTmpl: AssignTmplAction,
	editMode: boolean,
	unit: Unit,
}) {
	const color = getColor(get([id, 'body', 'unit', 'opaque_params'], templates));
	const label = get([id, 'body', 'name'], templates);

	const handleDelete = () =>
		assignTmpl({
			uids: [unit.uid],
			templateIds: without([id], assigned),
		}); // TODO: return useCallback

	return (
		<ThemedChip onDelete={editMode ? handleDelete : null} {...{color, label}} />
	);
}

function AssignedTmpls({templates, editMode, assignTmpl, unit}: Props) {
	if (!unit.assigned_templates) {
		return null;
	}

	const assigned = get(['assigned_templates'], unit);

	return (
		<ListItem>
			{map(
				id => (
					<AssignedTemplate
						key={id}
						{...{id, templates, assigned, assignTmpl, editMode, unit}}
					/>
				),
				assigned
			)}
		</ListItem>
	);
}

export default compose(withTemplates)(AssignedTmpls);
