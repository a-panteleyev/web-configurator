// @flow

import MicOn from '@material-ui/icons/Mic';
import MicOff from '@material-ui/icons/MicOff';
import SubUnit from './SubUnit';

export const Microphone = SubUnit({
	name: 'Microphone',
	iconOn: MicOn,
	iconOff: MicOff,
});

export default Microphone;
