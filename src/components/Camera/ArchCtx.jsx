// @flow

import * as React from 'react';
import {compose} from 'redux';
import noop from 'lodash/fp/noop';
import Grow from '@material-ui/core/Grow';
import IconButton from '@material-ui/core/IconButton';
import Clear from '@material-ui/icons/Clear';
import Storage from '@material-ui/icons/Storage';

import logger from 'utils/logger';
import {withUnit} from 'containers/Units';
import type {Unit} from 'containers/Units/type';

type Props = {
	unit: Unit,
	editMode: boolean,
	setEdit: Function,
	requestUnit: Function,
	rmUnit: Function,
};

export function ArchContext({
	editMode,
	unit,
	setEdit,
	requestUnit,
	rmUnit,
}: Props) {
	const handleEdit = React.useCallback(() => setEdit(unit), [unit]);
	const handleRm = React.useCallback(() => {
		logger.log('remove %s', unit.uid);
		rmUnit({units: [unit.uid]});
	}, []);

	// TODO: loading
	// TODO: disable on editMode == false
	// TODO: parent unit
	// TODO: need to sort out updating of component on response from server
	// TODO: consider generic RemoveUnit component
	//
	// props are coming from Streaming, which isn't aware of ArchiveContext
	// being child of MultimediaStorage
	React.useEffect(() => {
		if (unit.stripped) {
			requestUnit([unit]);
		}

		return noop;
	});

	return (
		<>
			<IconButton onClick={handleEdit}>
				<Storage />
			</IconButton>
			<Grow in={editMode}>
				<IconButton onClick={handleRm}>
					<Clear />
				</IconButton>
			</Grow>
		</>
	);
}

const ArchCtx = withUnit(ArchContext);
//
// export const ArchCtx = ({unit, ...rest}: {unit: Unit}) => (
// 	<SelectdArchCtx uid={unit.uid} {...rest} />
// );
