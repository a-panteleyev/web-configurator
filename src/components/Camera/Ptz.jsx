// @flow

import ControlCamera from '@material-ui/icons/ControlCamera';
import LocationDisabled from '@material-ui/icons/LocationDisabled';
import SubUnit from './SubUnit';

export const Ptz = SubUnit({
	name: 'Ptz',
	iconOn: ControlCamera,
	iconOff: LocationDisabled,
});

export default Ptz;
