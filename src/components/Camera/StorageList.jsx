// @flow

import * as React from 'react';
import {compose} from 'redux';
import get from 'lodash/fp/get';
import find from 'lodash/fp/find';
import StorageIcon from '@material-ui/icons/Storage';
import {withStyles} from '@material-ui/core/styles';

import {withType, unitType} from 'containers/Units';
import type {Unit} from 'containers/Units/types';

const styles = {};

type Props = {
	unit: Unit,
	cameraRef: string,
};

function Storage({unit, cameraRef}: Props) {
	const recording = React.useMemo(
		() => find(get(['properties', 'camera_ref', 'value_string']), unit),
		[unit, cameraRef]
	);

	return <StorageIcon color={recording ? 'primary' : 'default'} />;
}

function StorageList({units, ...rest}: {units: Array<Unit>}) {
	return units.map(unit => <Storage key={unit.uid} {...{unit, ...rest}} />);
}

export default compose(withType(unitType.STORAGE))(StorageList);
