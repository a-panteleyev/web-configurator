// @flow

import VolumeUp from '@material-ui/icons/VolumeUp';
import VolumeOff from '@material-ui/icons/VolumeOff';
import SubUnit from './SubUnit';

export const Speaker = SubUnit({
	name: 'Speaker',
	iconOn: VolumeUp,
	iconOff: VolumeOff,
});

export default Speaker;
