// @flow
import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import cn from 'classnames';
import flow from 'lodash/fp/flow';
import filter from 'lodash/fp/filter';
import find from 'lodash/fp/find';
import get from 'lodash/fp/get';
import getOr from 'lodash/fp/getOr';
import isEqual from 'lodash/fp/isEqual';
import map from 'lodash/fp/map';
import noop from 'lodash/fp/noop';
import {withStyles} from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import Grow from '@material-ui/core/Grow';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Popper from '@material-ui/core/Popper';
import Add from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Clear';
import Typography from '@material-ui/core/Typography';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';

import {unitType} from 'containers/Units';
import type {Unit} from 'containers/Units/types';
import AssignedTmpls from 'components/AssignedTemplates';
import UnitShifter from 'components/UnitShifter';
import logger from 'utils/logger';
import {getFactories} from 'utils/units';

const getAddress: Unit => string = getOr('…', [
	'properties',
	'address',
	'value_string',
]);
const getName: Unit => string = flow([
	get('units'),
	find(flow([get('type'), isEqual(unitType.CAM_CHAN)])),
	getOr('…', ['properties', 'display_name', 'value_string']),
]);
const getSub = type =>
	flow([get('units'), filter(flow([get('type'), isEqual(type)]))]);

const styles = theme => ({
	root: {
		backgroundColor: theme.palette.background.paper,
		margin: theme.spacing.unit,
		opacity: 1,
		transitionDuration: theme.transitions.duration.standard,
		transitionTimingFunction: theme.transitions.easing.easeIn,
	},
	content: {
		overflow: 'hidden',
	},
	expanded: {
		opacity: 0,
		pointerEvents: 'none',
		transitionDuration: theme.transitions.duration.standard,
		transitionTimingFunction: theme.transitions.easing.easeOut,
	},
	icon: {
		color: theme.palette.grey['500'], // TODO: add to theme
	},
	rm: {
		display: 'flex',
		flexDirection: 'column',
		backgroundColor: theme.palette.text.disabled,
	},
	title: {
		color: theme.palette.textColor,
		fontSize: 32,
		textAlign: 'right',
		position: 'absolute',
		top: -theme.spacing.unit * 3,
		left: theme.spacing.unit * 2,
	},
	add: {
		position: 'absolute',
		right: -theme.spacing.unit * 3,
		boxShadow: 'none',
		transition: [['transform', `${theme.transitions.duration.short}ms`]],
	},
});

const itemStyles = theme => ({
	caption: {
		position: 'absolute',
		top: theme.spacing.unit * 4,
	},
});
const FactoryItem = withStyles(itemStyles)(
	({
		classes,
		factory,
		setSubfactory,
	}: {
		classes: Object,
		factory: Factory,
		setSubfactory: Function,
	}) => {
		const handleCreate = React.useCallback(() => setSubfactory(factory), [
			factory,
		]);

		return (
			<MenuItem onClick={handleCreate}>
				<Typography variant="body2" color="textPrimary">
					{factory.type}
				</Typography>
				<Typography
					className={classes.caption}
					variant="caption"
					color="textSecondary"
				>
					{factory.ctx}
				</Typography>
			</MenuItem>
		);
	}
);

type Props = {
	children: React.Node,
	className: string,
	classes: {[string]: string},
	editMode: boolean,
	unit: Unit,
	setUnit: Unit => void,
	setSubfactory: Factory => void,
	hover: Object,
};

function CameraUnstyled({
	children,
	classes,
	className,
	unit,
	hover,
	...props
}: Props) {
	const {editMode, setSubfactory} = props;
	const displayName = `${unit.display_id}. ${getName(unit)}`;
	const address = getAddress(unit);

	return (
		<>
			<CardContent classes={{root: classes.content}}>
				<Typography variant="h5" noWrap onClick={() => console.log(unit)}>
					{displayName}
				</Typography>
				<Typography noWrap>
					{address}
				</Typography>
				<Typography variant="caption">
					<FormattedMessage id="unit.address" />
				</Typography>
			</CardContent>
			<Grid container className={cn(classes.list, className)} {...hover}>
				<AssignedTmpls {...{unit, editMode}} />
				{children}
			</Grid>
		</>
	);
}

export const Camera = compose(withStyles(styles))(CameraUnstyled);
export default {Camera};
