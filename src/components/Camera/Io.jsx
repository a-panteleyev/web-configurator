// @flow

import Input from '@material-ui/icons/SettingsInputComponent';
import SubUnit from './SubUnit';

export const Io = SubUnit({
	name: 'Io',
	iconOn: Input,
});

export default Io;
