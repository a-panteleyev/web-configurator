// @flow

import Icon from '@material-ui/icons/RecentActors';
import SubUnit from './SubUnit';

export const Microphone = SubUnit({
	name: 'Microphone',
	iconOn: Icon,
});

export default Microphone;
