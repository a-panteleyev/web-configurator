// @flow

import * as React from 'react';
import {FormattedMessage} from 'react-intl';
import {compose} from 'redux';
import cn from 'classnames';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import size from 'lodash/fp/map';
import endsWith from 'lodash/fp/endsWith';
import Badge from '@material-ui/core/Badge';
import HqIcon from '@material-ui/icons/HighQuality';
import Grow from '@material-ui/core/Grow';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Clear from '@material-ui/icons/Clear';
import MoreVert from '@material-ui/icons/MoreVert';

import {withUnits} from 'containers/Units';
import {selectArchCtx} from 'containers/Units/utils';
import AddFactory from 'components/AddFactory';
import logger from 'utils/logger';
import {findProp} from 'utils/props';
import {useAnchor} from 'utils/material';
import {several} from 'utils/misc';
import type {Unit as UnitT, Factory} from 'containers/Units/types';
import StorageList from './StorageList';

const findResolution = findProp('resolution');
const isHqStream = flow([get('uid'), endsWith('.0')]);

const streamStyles = {
	stream: {
		position: 'relative',
	},
};

const badgeStyles = theme => ({
	root: {
		margin: [
			[
				theme.spacing.unit * 2,
				0,
				theme.spacing.unit * 2,
				theme.spacing.unit / 2,
			],
		],
	},
	badge: {
		top: '50%',
		left: -theme.spacing.unit,
		width: theme.spacing.unit * 3,
	},
});
const textStyles = theme => ({
	root: {
		margin: [[0, 0, 0, theme.spacing.unit * 4]],
	},
});

const HqBadge = withStyles(badgeStyles)(Badge);
HqBadge.displayName = 'HqBadge';

const Resolution = withStyles(textStyles)(Typography);
Resolution.displayName = 'Resolution';

export const Streaming = ({
	unit,
	units,
	editMode,
	rmUnit,
	children,
	className,
	hover,
	setEdit,
	setSubfactory,
}: {
	children: React.Node,
	classes: Object,
	className: string,
	unit: UnitT,
	units: UnitT,
	editMode: boolean,
	rmUnit: Function,
	hover: Object,
	setEdit: Function,
	setSubfactory: Function,
}) => {
	const handleClick = React.useCallback(() => {
		logger.log('remove %s', unit.uid);
		rmUnit({units: [unit]});
	}, []);
	const handleEdit = React.useCallback(() => {
		setEdit(unit);
	}, []);
	const streamClasses = cn(className);

	return (
		<div className={streamClasses} {...hover}>
			<HqBadge
				invisible={!isHqStream(unit)}
				badgeContent={<HqIcon />}
				onClick={handleEdit}
			>
				<Resolution>
					{findResolution(unit)}
				</Resolution>
			</HqBadge>
			<Grow in={editMode}>
				<IconButton onClick={handleClick}>
					<Clear />
				</IconButton>
			</Grow>
			<AddFactory {...{unit, setSubfactory, enabled: editMode}} />
			{children}
		</div>
	);
};

export default compose(withStyles(streamStyles))(Streaming);
