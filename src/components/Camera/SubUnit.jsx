// @flow

import * as React from 'react';
import {FormattedMessage} from 'react-intl';
import cn from 'classnames';
import isEqual from 'lodash/fp/isEqual';
import update from 'lodash/fp/update';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';
import Tooltip from '@material-ui/core/Tooltip';
import withStyles from '@material-ui/core/styles/withStyles';

import type {Unit} from 'containers/Units/types';

const styles = theme => ({
	icon: {padding: theme.spacing.unit * 1.5},
	toggle: {
		transition: [
			['opacity', `${theme.transitions.duration.standard}ms`],
			['transform', `${theme.transitions.duration.standard}ms`],
		],
	},
	toggleInvisible: {
		opacity: 0,
		transform: 'scale(0.75)',
	},
	toggleVisible: {
		opacity: 1, // <Grid /> breaks <Switch />
		transform: 'scale(1)',
	},
	container: {
		display: 'inline-flex',
	},
});

export default function SubUnit({
	name,
	iconOn,
}: {
	name: string,
	iconOn: React.ComponentType<*>,
}): React.ComponentType<*> {
	const Component = withStyles(styles)(
		({
			children,
			classes,
			className,
			unit,
			editMode,
			setEdit,
			hover,
		}: {
			children: React.Node,
			classes: Object,
			className: string,
			unit: Unit,
			editMode: boolean,
			setEdit: Function,
			hover: Object,
		}) => {
			const Icon = iconOn;
			const isActive = isEqual('UNIT_STATUS_ACTIVE', unit.status);
			// const handleClick = React.useCallback(() => setEdit(unit), [unit]);

			return (
				<Tooltip title={<FormattedMessage id={`unit.type.${unit.type}`} />}>
					<div className={className} {...hover}>
						<Icon
							classes={{root: classes.icon}}
							color={isActive ? 'inherit' : 'disabled'}
						/>
						{children}
					</div>
				</Tooltip>
			);
		}
	);

	Component.displayName = name;

	return Component;
}
