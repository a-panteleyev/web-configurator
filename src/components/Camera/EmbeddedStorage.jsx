// @flow

import StorageOn from '@material-ui/icons/SdStorage';
import StorageOff from '@material-ui/icons/NoSim';
import SubUnit from './SubUnit';

export const EmbeddedStorage = SubUnit({
	name: 'EmbeddedStorage',
	iconOn: StorageOn,
	iconOff: StorageOff,
});

export default EmbeddedStorage;
