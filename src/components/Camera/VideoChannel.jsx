// @flow

import * as React from 'react';
import {compose} from 'redux';
import cn from 'classnames';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import last from 'lodash/fp/last';
import isEqual from 'lodash/fp/isEqual';
import Divider from '@material-ui/core/Divider';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import Add from '@material-ui/icons/Add';

import logger from 'utils/logger';
import {toMap, toList} from 'utils/props';
import {withUnits, unitType} from 'containers/Units';
import type {Unit} from 'containers/Units/types';
import AddFactory from 'components/AddFactory';
import Streaming from './Streaming';

const findStreamFactory = find(
	flow([get('type'), isEqual(unitType.CAM_STREAM)])
);
const styles = theme => ({
	item: {
		padding: 0,
	},
	disabled: {
		transform: 'scale(0)',
	},
	add: {
		marginTop: -theme.spacing.unit * 6,
	},
});

function VideoChannelUnstyled({
	addUnit,
	classes,
	children,
	className,
	unit,
	editMode,
	hover,
	setSubfactory,
	...rest
}: {
	addUnit: Function,
	classes: Object,
	children: React.Node,
	className: string,
	unit: Unit,
	editMode: boolean,
	hover: Object,
	setSubfactory: Function,
}) {
	const addStream = React.useCallback(() => {
		const factory = flow([findStreamFactory, toMap])(unit.factory);

		// TODO: Check unit for how many Streams already exist
		factory.properties.streaming_id.value_int32 = flow([
			get(['properties', 'streaming_id', 'enum_constraint', 'items']),
			last,
			get('value_int32'),
		])(factory);
		addUnit({
			units: [{uid: unit.uid, units: [toList(factory)]}],
		});
	}, [unit]);

	return (
		<>
			<Grid container direction="row" className={className} {...hover}>
				{children}
				<Divider />
			</Grid>
			<AddFactory
				{...{className: classes.add, unit, setSubfactory, enabled: editMode}}
			/>
			{/*
				{unit.units.map((stream, i) => (
					<Streaming
						key={`${stream.uid}-${i}`}
						unit={stream}
						{...{editMode, ...rest}}
					/>
				))}
			<Fab
				classes={{root: cn(classes.add, {[classes.disabled]: !editMode})}}
				size="small"
				onClick={addStream}
			>
				<Add />
			</Fab>
			*/}
		</>
	);
}

export const VideoChannel = withStyles(styles)(VideoChannelUnstyled);

export default VideoChannel;
