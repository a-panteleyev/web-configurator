// @flow

import * as React from 'react';
import cn from 'classnames';
import filter from 'lodash/fp/filter';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import isNil from 'lodash/fp/isNil';
import map from 'lodash/fp/map';
import negate from 'lodash/fp/negate';
import * as colors from '@material-ui/core/colors';
import {withStyles} from '@material-ui/core/styles';

const colorSet = flow([map(get('500')), filter(negate(isNil))])(colors);
const setStyles = theme => ({
	set: {
		marginTop: theme.spacing.unit * 2,
	},
});
const styles = theme => ({
	color: {
		display: 'inline-block',
		borderRadius: theme.shape.borderRadius,
		margin: theme.spacing.unit / 4,
		width: theme.spacing.unit * 2,
		height: theme.spacing.unit * 2,
	},
	spot: {
		borderWidth: [[theme.spacing.unit / 4, 0]],
		borderColor: 'transparent',
		borderRadius: theme.shape.borderRadius,
		borderStyle: 'solid',
		cursor: 'pointer',
		display: 'inline-flex',
		'&:hover': {
			borderColor: theme.palette.common.black,
		},
		transition: [['border', `${theme.transitions.duration.shortest}ms`]],
	},
	spotSelected: {
		borderColor: theme.palette.grey[500],
	},
});

const ColorSpot = withStyles(styles)(
	({
		classes,
		color,
		onClick,
		selected,
	}: {
		classes: Object,
		color: $Values<colorSet>,
		onClick: Function,
		selected: boolean,
	}) => (
		<span
			className={cn(classes.spot, {[classes.spotSelected]: selected})}
			onClick={onClick}
		>
			<span className={classes.color} style={{backgroundColor: color}} />
		</span>
	)
);

function ColorPicker({classes}: {classes: Object}) {
	const [selected, setSelected] = React.useState(colors.common.black);

	return (
		<div className={classes.set}>
			<input type="hidden" name="color" value={selected} />
			{map(color => {
				const handleSelect = React.useCallback(() => setSelected(color), [
					color,
				]);
				const isSelected = selected === color;

				return (
					<ColorSpot
						key={color}
						color={color}
						onClick={handleSelect}
						selected={isSelected}
					/>
				);
			}, colorSet)}
		</div>
	);
}

export default withStyles(setStyles)(ColorPicker);
