import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import ru from 'react-intl/locale-data/ru';

addLocaleData(en);
addLocaleData(ru);

const defaultLocale = 'en';
const supportedLocales = [defaultLocale];
const browserLocale =
  window.navigator.language || window.navigator.userLanguage;
const userLocale =
  typeof browserLocale === 'string' ? browserLocale.slice(0, 2) : '';
const localeIsSupported = supportedLocales.indexOf(userLocale) !== -1;

export default (localeIsSupported ? userLocale : defaultLocale);
