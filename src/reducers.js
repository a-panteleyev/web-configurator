// @flow
import {combineReducers} from 'redux';

import globalReducer from 'containers/App/reducer';
import unitsReducer from 'containers/Units/reducer';
import templatesReducer from 'containers/Templates/reducer';
import workbenchReducer from 'containers/Workbench/reducer';
import searchReducer from 'containers/Search/reducer';
import selectionReducer from 'containers/Selection/reducer';

export default combineReducers({
	global: globalReducer,
	units: unitsReducer,
	templates: templatesReducer,
	workbench: workbenchReducer,
	selection: selectionReducer,
	search: searchReducer,
});
